﻿using UnityEditor;
using UnityEngine;

public enum PlayerMovementStatus
{
    Idle,
    Walking,    //This is the normal speed
    Running,    //This is the increased speed, archieved when the player has pressed the move button for a x time.
    Climbing
};

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour
{

    #region Public Fields
    [Header("Movement Variables")]
    [Tooltip("The rate we are moving normal.")]
    public float MovementNormalSpeed = 2f;

    [Tooltip("The rate we will move when we are running.")]
    public float MovementRunSpeed = 4f;

    [Tooltip("The rate we will move up/down on the stairs.")]
    public float VerticalMovementSpeed = 2f;

    [Tooltip("The amount of impulse we do when we need to jump.")]
    public float JumpImpulse = 2f;

    [Header("Speed Modifiers")]
    [Tooltip("The time the player have to keep the movement button pressed to increase the movement speed")]
    public int TimeUntilSpeedIncrease = 4;

    #endregion
    
    #region Private Fields
    private bool onGround = true;               //Track if we're on the ground or not.
    private bool onStairs = false;              //We use this to check if we are close to a stair, so we can use it.
    private bool canJump = true;                //TODO:Do we wanna some kind of cooldown here ?
    
    private bool movementSpeedIncreaseBoolean = false; //We use this to track if we have to increase or not the movement speed.
    private bool movementSpeedInputBoolean = false;    //We use this to track for input.
    private float movementSpeedIncreaseTimer;          //We use this to track the timer internal.

    private float defaultGravityScale;

    private float movePlayerHorizontal;
    private float movePlayerVertical;

    private Rigidbody2D _rigidbody2D;

    #endregion

    #region Protected / Internal Fields

    internal PlayerMovementStatus MovementStatus;

    #endregion

    #region Mono

    internal void PlayerMovementAwake()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();

        defaultGravityScale = _rigidbody2D.gravityScale;

        MovementStatus = PlayerMovementStatus.Idle;    
    }

    //This is our movement core.
    internal void PlayerMovementUpdate()
    {
        Debug.DrawRay(transform.position, Vector2.up * 10);
   
        //This control the player movement based in input's.
        ControlHorizontalMovement();
        ControlVerticalMovement();

        //This controls the status of the player movement.
        ControlMovementStatus();
    }

    #endregion

    #region Movement Status

    //TODO: We need to evaluate the velocity to change between walking and run.
    private void ControlMovementStatus()
    {
        if (onStairs)
        {
            MovementStatus = PlayerMovementStatus.Climbing;
        }
        else if (_rigidbody2D.velocity != new Vector2(0, 0))
        {
            //We are moving.
            MovementStatus = PlayerMovementStatus.Walking;
        }
    }

    #endregion

    #region Horizontal Movement

    private void ControlHorizontalMovement()
    {
        MovementSpeedIncreaseController();

        movePlayerHorizontal = Input.GetAxis("Horizontal");

        //Changing the velocity based in the speed increase control.
        if (movementSpeedIncreaseBoolean)
        {
            _rigidbody2D.velocity = new Vector2(movePlayerHorizontal * MovementRunSpeed, _rigidbody2D.velocity.y);
        }
        else if (!movementSpeedIncreaseBoolean)
        {
            _rigidbody2D.velocity = new Vector2(movePlayerHorizontal * MovementNormalSpeed, _rigidbody2D.velocity.y);
        }
    }

    #region Horizontal Movement Modifiers

    private void MovementSpeedIncreaseController()
    {
        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D))
        {
            //If we pres any movement button we change the bool to true to start work on the timer.
            if (!movementSpeedInputBoolean)
            {
                movementSpeedInputBoolean = true;
            }
        }
        else if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
        {
            //We reset all the modifiers when the button is released.
            movementSpeedIncreaseTimer = 0f;
            movementSpeedIncreaseBoolean = false;
        }

        //If this is true, we let the timer work.
        if (movementSpeedInputBoolean)
        {
            MovementSpeedIncreaseBasedOnInput();
        }
    }

    //This controls our timer to increase the speed. This should run when we pressed 
    //any horizontal movement down.
    private void MovementSpeedIncreaseBasedOnInput()
    {
        movementSpeedIncreaseTimer += Time.deltaTime;

        if (movementSpeedIncreaseTimer >= TimeUntilSpeedIncrease)
        {
            movementSpeedIncreaseBoolean = true;
        }
    }
        
    #endregion

    #endregion

    #region Vertical Movement

    private void ControlVerticalMovement()
    {
        //This controls our jump.
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }

        StairMovement();
    }

    private void Jump()
    {
        if (onGround)
        {
            canJump = false;
            _rigidbody2D.AddForce(Vector2.up * JumpImpulse, ForceMode2D.Impulse);
        }
    }

    private void StairMovement()
    {
        //We don't have a collision, we have nmothign to do here.
        if (!onStairs)
        {
            //Make sure our gravityScale is the default when we are not on stairs.
            if (_rigidbody2D.gravityScale != defaultGravityScale)
                _rigidbody2D.gravityScale = defaultGravityScale;
            
            //If we are not colliding with the stair we let the gravity handle our y velocity.
            return;    
        }
        
        movePlayerVertical = Input.GetAxis("Vertical");

        //Make this 0 so we don't have any effect when we are in the stairs.
        _rigidbody2D.gravityScale = 0f;

        _rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x, movePlayerVertical*VerticalMovementSpeed);
    }

    #endregion

    #region Collision

    //We need to check for collisions.
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Ground") || col.gameObject.CompareTag("Objects"))
        {
            //TODO:We should check for the angle here, so we can do some wall jump stuff.
            onGround = true;
        }
    }

    void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Ground") || col.gameObject.CompareTag("Objects"))
        {
            if (onGround)
                onGround = false;
        }
    }

    //TODO:Do a litle clean up here.
    //We are mainly using this for DEBUG stuff.
    void OnCollisionStay2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Ground") || col.gameObject.CompareTag("Objects"))
        {
            if (!onGround)
                onGround = true;

            //Debug.Log("Collision Angle: " + Vector3.Angle(Vector2.up, col.contacts[0].normal));
        }
    }

    #endregion

    //We use this to get the currentAcceleration.
    internal float CurrentMovePlayer()
    {
        return movePlayerHorizontal;
    }

    //We use this to track if we can move up/down on the stairs.
    //This is called in the StairsCollision.
    public void OnStair(bool onStair)
    {
        onStairs = onStair;
    }

    //Maybe we should make this internal.
    public bool GetStairCondition()
    {
        return onStairs;
    }
}