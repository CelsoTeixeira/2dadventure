﻿using UnityEngine;
using System.Collections;

public class PlayerAnimator : MonoBehaviour
{
    private Animator _animator;
    private PlayerMovement _playerMovement;

    private bool facingRight;               //We  use this to control the orientation of the sprite.

    internal void PlayerAnimatorAwake()
    {
        //We try to get the component in the object, if we fail we get in the children.
        _animator = GetComponent<Animator>();               

        if (_animator == null)
            _animator = GetComponentInChildren<Animator>();
       
        _playerMovement = GetComponent<PlayerMovement>();
    }

    internal void PlayerAnimatorUpdate()
    {
        ControlAnimation();
        FacingControl();
    }

    private void ControlAnimation()
    {
        ControlHorizontalAnimation();
        ControlVerticalAnimation();
    }

    private void ControlHorizontalAnimation()
    {
        _animator.SetFloat("MovementSpeed", Mathf.Abs(_playerMovement.CurrentMovePlayer()));    
    }

    //We control the vertical movement here.
    private void ControlVerticalAnimation()
    {
        _animator.SetBool("OnStairs", _playerMovement.GetStairCondition());
    }

    private void FacingControl()
    {
        if (_playerMovement.CurrentMovePlayer() < 0 && !facingRight)
        {
            Flip();
        }
        else if (_playerMovement.CurrentMovePlayer() > 0 && facingRight)
        {
            Flip();
        }
    }

    private void Flip()
    {
        facingRight = !facingRight;

        Vector3 localScale = transform.localScale;
        localScale.x *= -1;
        transform.localScale = localScale;
    }
}