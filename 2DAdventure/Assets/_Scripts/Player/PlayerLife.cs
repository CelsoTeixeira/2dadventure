﻿using UnityEngine;
using UnityEngine.UI;

public enum PlayerLifeStatus
{
    None,
    Alive,
    Dead
};

public class PlayerLife : MonoBehaviour
{
    public Transform RespawnPoint;

    public int MaxHealth = 100;

    public int HealthOnRess = 60;

    //We use this to get the currentHealth if we need anywhere.
    [HideInInspector]   //We don't need to show this in the inspector.
    public float CurrentHealth { get { return currentHealth; } }

    public GameObject UILifeBar;

    //We don't allow access to it outside from this class.
    private float currentHealth;

    private Image _lifeBarImage;


    internal PlayerLifeStatus LifeStatus = PlayerLifeStatus.None;


    internal void PlayerLifeAwake()
    {
        LifeStatus = PlayerLifeStatus.Alive;

        _lifeBarImage = UILifeBar.GetComponent<Image>();

        currentHealth = MaxHealth;
        _lifeBarImage.fillAmount = currentHealth / 100;
    }

    #region Private Methods

    private void ControlLife()
    {
        if (currentHealth < 0)
        {
            //We ded
            LifeStatus = PlayerLifeStatus.Dead;

            this.gameObject.transform.position = RespawnPoint.position;

            currentHealth = MaxHealth;
            _lifeBarImage.fillAmount = currentHealth / 100;
        }
    }

    #endregion

    #region Public Methods

    public void ApplyDamage(float damage)
    {
        
        currentHealth += damage;

        _lifeBarImage.fillAmount = currentHealth / 100;

        //We only check the life if we need it, we don't check it everyframe.
        ControlLife();
    }

    #endregion

    #region Protected / Internal Methods

    internal void ResetPlayer()
    {
        currentHealth = HealthOnRess;
        LifeStatus = PlayerLifeStatus.Alive;
    }

    #endregion
}