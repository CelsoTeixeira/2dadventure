﻿/*  This class handles our initializations and updates.
    We don't need this... (?)
    
    */


using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlayerMovement))]
[RequireComponent(typeof(PlayerAnimator))]
[RequireComponent(typeof(PlayerLife))]
public class PlayerController : MonoBehaviour
{

    private PlayerMovement _playerMovement;
    private PlayerAnimator _playerAnimator;
    private PlayerLife _playerLife;

    void Awake()
    {
        _playerMovement = GetComponent<PlayerMovement>();
        _playerAnimator = GetComponent<PlayerAnimator>();
        _playerLife = GetComponent<PlayerLife>();

        _playerMovement.PlayerMovementAwake();
        _playerAnimator.PlayerAnimatorAwake();
        _playerLife.PlayerLifeAwake();
    }

    void Update()
    {
        if (_playerMovement != null)
        {
            _playerMovement.PlayerMovementUpdate();;
        }

        if (_playerAnimator != null)
        {
            _playerAnimator.PlayerAnimatorUpdate();
        }
        

    }
}
