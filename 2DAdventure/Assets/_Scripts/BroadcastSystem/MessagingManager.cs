﻿using UnityEngine;
using System;
using System.Collections.Generic;

/*  We should make a different subscriber for everything we need to control.
 */

public class MessagingManager : MonoBehaviour
{
    #region Singleton Pattern

    public static MessagingManager Instance { get; private set; }

    private void SingletonSetup()
    {
        if(Instance != this && Instance != null)
            Destroy(gameObject);

        Instance = this;
    }

    #endregion

    private List<Action> subscribers = new List<Action>();

    private Dictionary<string, Action> subscribersDictionary = new Dictionary<string, Action>();

    #region Mono

    void Awake()
    {
        SingletonSetup();
    }

    #endregion

    #region Public Methods

    //We use this to subscribe
    public void Subscribe(Action subscriber)
    {
        Debug.Log("Subscriber registered", gameObject);
        subscribers.Add(subscriber);
    }

    //We use this to unsubscribe
    public void UnSUbscribe(Action subscriber)
    {
        Debug.Log("Subscriber removed");
        subscribers.Remove(subscriber);
    }

    //We use this to clear the subscribers list
    public void ClearAllSubscribers()
    {
        subscribers.Clear();
    }

    //We use this to let every Action in the subscribers know what happened.
    public void Broadcast()
    {
        Debug.Log("Broadcast requested, No of subscribers: " + subscribers.Count);
    
        //TODO: We need to test this iteration.

        //We make the subscribers to array to kind lock it and we don't get errors if
        //an subscriber is removed from the list when we are iterating through the list.
        Action[] actions = subscribers.ToArray();

        for (var x = 0; x >= actions.Length; x++)
        {
            Action subscribe = actions[x];
            subscribe();
        }
    }

    #region Specific Broadcast
    //What we are trying to do here is to only trigger specific actions when we want to broadcast something.
    public void AddOnDictionary(string id, Action actionToAdd)
    {
        if (!subscribersDictionary.ContainsKey(id))
        {
            Debug.Log("We are adding a new Action in the Dictionary with the following key: " + id);
            subscribersDictionary.Add(id, actionToAdd);
        }
        else
        {
            Debug.Log("We already have a key with the same name.");
        }
    }

    public void BroadcastSpecificAction(string id)
    {

    }
    #endregion
    #endregion
}
