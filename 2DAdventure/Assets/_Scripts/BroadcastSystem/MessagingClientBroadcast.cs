﻿using UnityEngine;
using System.Collections;


/*  This script is attached in some gameObject, if the OnCollisionEnter2D(condition) is called
 *  we Broadcast from the MessagingManager.
 */

public class MessagingClientBroadcast : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D col)
    {
        MessagingManager.Instance.Broadcast();
    }
}
