﻿using UnityEngine;
using System.Collections;

/*  With this class, we subscribe the action(method) we want
 *  to be used when the broadcast is active.
 *  
 *  We subscribe this method "ThePlayerIsTryingToLeave" on the 
 *  MessagingManager.
 *  When the Broadcast is called, he will call our method(Action)
 *  that we subscribed.
 */

public class MessagingClientReceiver : MonoBehaviour 
{
    void Start()
    {
        //Here we regitre the action we want to broadcast.
        MessagingManager.Instance.Subscribe(ThePlayerIsTryingToLeave);
    }

    //The action we want to broadcast.
    
    void ThePlayerIsTryingToLeave()
    {
        Debug.Log("The player is trying to leave");
    }
}