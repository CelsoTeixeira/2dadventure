﻿using UnityEngine;
using System.Collections;

/*  How can we change the keybinds in real time ?? We need to pass a string to deal with this.
 *  We need an GuiPanel, in this Panel we can call some type of function and change the keybinds. 
 */

public class KeyBindsSetup : MonoBehaviour
{
    #region Singleton pattern

    public static KeyBindsSetup Instance { get; private set; }

    private void SingletonSetup()
    {
        if (Instance != this && Instance != null)
            Destroy(gameObject);

        Instance = this;
    }

    #endregion

    //We don't need this to be public, I don't want this to be public!
    public KeyCode[] KeyBindsCodes = new KeyCode[4];

    public KeyCode ActionKeyCode = KeyCode.F;

    #region Mono

    void Awake()
    {
        SingletonSetup();

        SetupKeyBinds();
    }

    void Update() { }

    #endregion

    #region Default properties

    //This is our default keyBinds. We call this on Awake to be the default every time, maybe we should check playerprefs
    //and not set default every time.
    private void SetupKeyBinds()
    {
        //We need to change this to accept strings.
        KeyBindsCodes[0] = KeyCode.Alpha1;
        KeyBindsCodes[1] = KeyCode.Alpha2;
        KeyBindsCodes[2] = KeyCode.Alpha3;
        KeyBindsCodes[3] = KeyCode.Alpha4;

        ActionKeyCode = KeyCode.F;
    }

    #endregion

    #region Public Access

    //We can call this on some GUI buttons. We need to pass another thing diferent from KeyCode.
    public void ChangeKeyBind(KeyCode code, int slot)
    {
        if (slot > KeyBindsCodes.Length)
        {
            Debug.Log("The int passed is higher than the KeyBindsCode array.");
            return;
        }

        KeyBindsCodes[slot] = code;
        Debug.Log("New KeyCode for KeyBind: " + code + " / " + slot);
    }

    #endregion
}