﻿using UnityEngine;
using System.Collections;

public class HandleInput : MonoBehaviour
{
    #region Singleton Pattern

    public static HandleInput Instance { get; private set; }

    private void SingletonSetup()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        
        Instance = this;
    }

    #endregion

    #region Private Fields

    //Reference for the basic attack component. This should be handled in the action bar.
    private BasicAttack _basicAttack;       

    //We need a reference for the action bar, this will call function to launch shit.
    private ActionBar _actionBar;

    //This shoukd be equal the KeyBindsSetup.
    private KeyCode[] inputKeyCodes;    //Probly there is a better way to do this. Right now I'm just cloning the same array from the KeyBinds setup.

    #endregion

    #region Mono

    void Awake()
    {
        SingletonSetup();       //We set up te Singleton.
        
        //TODO:Clean this!
        //We don't need this to be here, we should call the action bar to handle this stuff.
        _basicAttack = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<BasicAttack>();      //We get the reference.

        if (_basicAttack == null)
        {
            Debug.Log("Basic Attack is null");
        }
    }

    void Start()
    {
        //We are setting the instance on Awake(), if we call this on Awake() we can get null reference. Call this on Start() to avoid it.
        inputKeyCodes = KeyBindsSetup.Instance.KeyBindsCodes;
    }

    void Update()
    {
        //We are going to accept Q or MouseButton0 to cast the basic attack.
        if (Input.GetMouseButtonDown(0))
        {
            //When the mouse button is pressed we do a raycast with the mouse position.
            HandleMousePress();
        }

        ////TODO: Clean this, the static method to get enemies in a Circle is working.
        //if (Input.GetKeyDown(KeyCode.Z))
        //{
        //    Debug.Log("Z pressed.");

        //    GameObject collider2Ds = GetClosestEnemy.ClosestEnemy(GameObject.FindGameObjectWithTag("Player").transform,
        //        10f, "Enemy", 8);
        //}

        HandleButtonPressed();
    }

    #endregion

    #region Private methods

    //This occurs every frame, but we only enter the ButtonKeyDown if its down.
    private void HandleButtonPressed()
    {
        //We use this for a fail check.
        if (inputKeyCodes == null)
        {
            inputKeyCodes = KeyBindsSetup.Instance.KeyBindsCodes;
            return;
        }

        //This is our main loop, it go throug our keycode array and handle button key down.
        for (var x = 0; x < inputKeyCodes.Length; x++)
        {
            ButtonKeyDown(inputKeyCodes[x], x);
        }
    }
    
    //Just for reuses porpuses.
    private void ButtonKeyDown(KeyCode code, int slotReference)
    {
        //TODO:Implement action bar call here.
        if (Input.GetKeyDown(code))
        {

        }
    }

    //This only occurs when we press the mouse.
    private void HandleMousePress()
    {
        //We do a Raycast2D and save the information in hit.
        //We are Raycasting only in the Entitys layer, maybe we should go to default if we
        //we need to check with other stuff.
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition),
            Camera.main.ScreenToWorldPoint(Input.mousePosition), 1 << 8);

        //This are not giving a null error anymore. Probly is good now.
        if (hit)    //We check to see if the RaycastHit is true or false.
        {
            //We maybe need to check if the enemy is behind something before we start the attack. 
            //We can do another Raycast from the player to the enemy and see if we hit the enemy or not.
            if (hit.collider.gameObject.CompareTag("Enemy"))
            {
                Debug.Log("We hitted the enemy, we need to check for distance before anything.");
                //We should acess the ActionBar and handle this there.

                //Now we need to call the basic attack, and pass this enemy.
                _basicAttack.AssignTarget(hit.collider.gameObject);
            }
            else if (hit.collider.gameObject.CompareTag("Objects")) //We need to change this to be handled everyframe if we have an object.
            {
                Vector3 mousePoistionVector3 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                mousePoistionVector3 = new Vector3(mousePoistionVector3.x, mousePoistionVector3.y, -0.9f);

                hit.collider.gameObject.transform.position = mousePoistionVector3;
            }
        }
    }

    //TODO: We need to do a method to handle when the mouse button is pressed. Do a different class for this?
    private void HandleMovingObject()
    {
    }

    #endregion

    #region Public methods

    public void SetupKeybinds(KeyCode[] codes)
    {
        Debug.Log("We are refreshing the KeyBinds on the HandleInput");
        inputKeyCodes = codes;
    }

    #endregion

}