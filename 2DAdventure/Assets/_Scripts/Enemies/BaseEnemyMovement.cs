﻿using UnityEngine;
using System.Collections;

public enum EnemyMovement
{
    Idle,
    Patrol,
    FollowTarget
};

public class BaseEnemyMovement : MonoBehaviour 
{
    protected internal FollowTargetMovement _FollowTargetMovement;
    protected internal WaypointMovement _WaypointMovement;

    //We are going to use this to handle the movement stuff.
    protected internal delegate void MovementUpdate();
    protected internal MovementUpdate _MovementUpdate;

    protected internal delegate void MovementFixedUpdate();
    protected internal MovementFixedUpdate _MovementFixedUpdate;

    //TODO: Change this to protected internal
    public EnemyMovement _EnemyMovementState = EnemyMovement.Idle;

    protected internal void EnemyMovementAwake()
    {
        _FollowTargetMovement = GetComponentInChildren<FollowTargetMovement>();
        _WaypointMovement = GetComponentInChildren<WaypointMovement>();
    }

    protected internal void EnemyMovementUpdate()
    {
        if (_MovementUpdate != null)
        {
            _MovementUpdate();        
        }
    }

    protected internal void EnemyMovementFixedUpdate()
    {
        if (_MovementFixedUpdate != null)
        {
            _MovementFixedUpdate();
        }
    }

    public void ChangeMovementStatus(EnemyMovement stateToChange)
    {
        Debug.Log("Changing enemy movement state, FROM:" + _EnemyMovementState + " / TO: " + stateToChange);

        //We actualize the state.
        _EnemyMovementState = stateToChange;

        //Clear the delegates.
        ClearDelegates();

        //We add new methods in the delegate based on the state we are going.

        if (_EnemyMovementState == EnemyMovement.Patrol)
        {
            //We subscribe a new one.
            _MovementFixedUpdate += _WaypointMovement.WaypointMovementFixedUpdate;

            //We trigger the movement.
            _WaypointMovement.TriggerMovement();
        }

        if (_EnemyMovementState == EnemyMovement.FollowTarget)
        {
            //We add each method from the movement to each delegate.
            _MovementUpdate += _FollowTargetMovement.FollowMovementUpdate;
            _MovementFixedUpdate += _FollowTargetMovement.FollowMovementFixedUpdate;

            //We trigger the movement.
            _FollowTargetMovement.TriggerMovement();
        }
    }

    //We call this to make sure we have emptys delegates each time we subscribe a new method to it.
    private void ClearDelegates()
    {
        _MovementUpdate = null;
        _MovementFixedUpdate = null;
    }
}