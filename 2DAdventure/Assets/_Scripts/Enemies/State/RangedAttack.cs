﻿using UnityEngine;
using System.Collections;

public enum RangedAttackState
{
    Cooldown,
    ReadyToAttack,
    Aiming,
    Attack
}

public class RangedAttack : MonoBehaviour 
{
    [Header("Ranged Attack variables")]
    public float AtackDamage = 10f;
    public float DistanceToAttack = 6f;
    public float TimeBetweenAttack = 2f;

    public Transform MuzzlePosition;
    public GameObject ProjectilePrefab;

    public Vector2 AmountOfForceToApply;

    private bool _canAttack = true;
    private float _cooldownTimer = 0f;

    public RangedAttackState _attackState = RangedAttackState.Cooldown;

    private GameObject _playerGameObject;
    
    private delegate void AttackUpdate();
    private AttackUpdate _attackUpdate;


    private bool _facingRight = true;

    private void Awake()
    {
        _playerGameObject = GameObject.FindGameObjectWithTag("Player");
    }

    internal void RangedAttackUpdate()
    {
        //We only run the delegate if its not null.
        if (_attackUpdate != null)
        {
            _attackUpdate();
        }
        else
        {
            //This is kind of a fail safe check to not stay frozen in the null.
            ChangeState(_attackState);
        }
    }

    private void CooldownTimerHandler()
    {
        _cooldownTimer += Time.deltaTime;

        if (_cooldownTimer >= TimeBetweenAttack)
        {
            _canAttack = true;
            ChangeState(RangedAttackState.ReadyToAttack);
        }
    }

    private void ReadyToAttack()
    {
        if (_canAttack)
        {
            if (Vector3.Distance(this.gameObject.transform.position, _playerGameObject.transform.position) <
                DistanceToAttack)
            {
                ChangeState(RangedAttackState.Attack);        
            }
        }
    }

    private void AttackHandler()
    {
        GameObject temp = Instantiate(ProjectilePrefab, MuzzlePosition.position, Quaternion.identity) as GameObject;
        ProjectileCollisionCheck check = temp.GetComponent<ProjectileCollisionCheck>();
        
        //We change the amount of damage we will apply on the player.
        check.Damage = AtackDamage;
        //The amount of force we will add on the rigidbody.
        if (this.gameObject.transform.position.x < _playerGameObject.transform.position.x)
        {
            if (!_facingRight)
            {
                Flip();
            }

            check.AddForce(AmountOfForceToApply);    
        }
        else if (this.gameObject.transform.position.x > _playerGameObject.transform.position.x)
        {
            if (_facingRight)
            {
                Flip();
            }

            check.AddForce(-AmountOfForceToApply);
        }
        
        _cooldownTimer = 0f;
        _canAttack = false;

        Debug.Log("Enemy ranged attack!");
        
        ChangeState(RangedAttackState.Cooldown);
    }

    //TODO:Take this off here.
    private void Flip()
    {
        _facingRight = !_facingRight;

        Vector3 theScale = this.gameObject.transform.localScale;

        theScale.x *= -1;

        this.gameObject.transform.localScale = theScale;
    }

    internal void ChangeState(RangedAttackState stateToChange)
    {
        Debug.Log("RANGED STATE- Changing enemy ranged attack state, FROM: " + _attackState + " / TO: " + stateToChange);

        _attackUpdate = null;

        switch (stateToChange)
        {
            //We could do a secondary delegate to hold what we need to check every frame.
            case RangedAttackState.Cooldown:
                _attackUpdate += CooldownTimerHandler;
                break;

            case RangedAttackState.ReadyToAttack:
                _attackUpdate += ReadyToAttack;
                break;

            case RangedAttackState.Attack:
                _attackUpdate += AttackHandler;
                break;
        }

        _attackState = stateToChange;
    }
}