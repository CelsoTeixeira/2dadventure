﻿using UnityEngine;
using System.Collections;

public enum MeleeAttackState
{
    Cooldown,
    ReadyToAttack,
    Attack
};

public class MeleeAttack : MonoBehaviour 
{
    [Header("Attack Variables")]
    [Tooltip("The amount of damage when we apply it to the player.")]
    public float AttackDamage = 10f;
    [Tooltip("Distance to perform the attack.")]
    public float DistanceToAttack = 2f;
    [Tooltip("The time between each attack.")]
    public float TimeBetweenAttacks = 2f;

    private bool _canAttack = true;
    private float _cooldownTimer = 0f;

    //TODO: Change this to protected or private.
    public MeleeAttackState _attackState = MeleeAttackState.ReadyToAttack;

    private GameObject _playerGameObject;
    private PlayerLife _playerLife;

    private delegate void AttackUpdate();
    private AttackUpdate _AttackUpdate;

    private void Awake()
    {
        _playerGameObject = GameObject.FindGameObjectWithTag("Player");
        _playerLife = _playerGameObject.GetComponent<PlayerLife>();
    }

    internal void MeleeAttackUpdate()
    {
        if (_AttackUpdate != null)
        {
            _AttackUpdate();
        }
        else
        {
            ChangeState(_attackState);
        }
    }

    private void CooldownTimerHandler()
    {
        _cooldownTimer += Time.deltaTime;   //We increase the cooldown.

        if (_cooldownTimer >= TimeBetweenAttacks)   //We check for the max timer.
        {
            _canAttack = true;  //We can attack.
            ChangeState(MeleeAttackState.ReadyToAttack);    //We change the state.
        }
    }

    private void ReadyToAttack()
    {
        //TODO:We can expand here.

        //If we can attack, just move to the attack.
        if (_canAttack)
        {

            ChangeState(MeleeAttackState.Attack);
        }
    }

    //TODO: Better evaluation when we do the attack.
    private void AttackHandler()
    {
        _playerLife.ApplyDamage(-AttackDamage);     //We apply the damage.

        //Reset variables.
        _cooldownTimer = 0f;    //We reset the cooldown.
        _canAttack = false;     //We reset the canAttack bool.

        //TODO: Trigger animation here.
        Debug.Log("Enemy melee attack");
        
        //Change the state.
        ChangeState(MeleeAttackState.Cooldown);     //We change the state.
    }

    internal void ChangeState(MeleeAttackState stateToChange)
    {
        Debug.Log("MELEE STATE- Changing enemy melee attack state, FROM: " + _attackState + " / TO: " + stateToChange);

        _AttackUpdate = null;  //We reset the delegate.

        //We assign new delegates based on the State we are going to go.
        switch (stateToChange)
        {
            case MeleeAttackState.Cooldown:
                _AttackUpdate += CooldownTimerHandler;
                break;

            case MeleeAttackState.ReadyToAttack:
                _AttackUpdate += ReadyToAttack;
                break;

            case MeleeAttackState.Attack:
                _AttackUpdate += AttackHandler;
                break;
        }
    }
}