﻿using UnityEngine;
using System.Collections;

public class ProjectileCollisionCheck : MonoBehaviour
{
    //This can me changed when we instantiate this.
    public float Damage = 10f;

    //The time we will destroy the GameObject after we instantiate here.
    public float TimeToAutoDestroy = 6f;

    public float TimeToAutoDestroyAfterWeHit = 1f;

    private PlayerLife _playerLife;
    private Rigidbody2D _rigidbody2D;

    void Awake()
    {
        //Cache references.
        _playerLife = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerLife>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
        
        //We make sure we destroy this after the TimeToAutoDestroy.
        //Destroy(this.gameObject, TimeToAutoDestroy);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            Debug.Log("Contact with the player, will apply hit.");
            _playerLife.ApplyDamage(-Damage);
        }
        else if (!col.gameObject.CompareTag("Enemy"))
        {
            //After we checked for collision we destroy the GameObject.
            //If it don't collide with the player we don't care, we will just destroy the Projectille GameObject.
            Destroy(this.gameObject, TimeToAutoDestroyAfterWeHit);        
        }
    }



    public void AddForce(Vector2 forceToAdd)
    {
        if (_rigidbody2D == null)
        {
            Debug.Log("We don't have a Rigidbody attached on this GameObject, we can't add force.");
            return;
        }

        //We add the force here with the vector2 passed.
        _rigidbody2D.AddForce(forceToAdd, ForceMode2D.Impulse);
    }   
}