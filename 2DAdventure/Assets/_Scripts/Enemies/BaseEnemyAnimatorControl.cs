﻿using UnityEngine;
using System.Collections;

public class BaseEnemyAnimatorControl : MonoBehaviour
{
    protected Animator _Animator;

    protected void Awake()
    {
        _Animator = GetComponent<Animator>();
    }

    protected void TriggerAnimation()
    {
        
    }
}