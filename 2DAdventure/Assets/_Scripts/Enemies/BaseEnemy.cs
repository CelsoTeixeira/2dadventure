﻿using UnityEngine;

//We control the state of each enemy.
//Do we need an more specific State condition, like a MeleeAttack enum that have 
//more definitions inside this MeleeAttack
public enum EnemyStates
{
    None, 
    Idle,
    MeleeAttack, 
    RangedAttack,
    Movement
};

public enum EnemyType
{
    None,
    Melee,
    Ranged
}

public class BaseEnemy : MonoBehaviour
{
    [Header("Base Enemy properties")]
    [Tooltip("The name of the enemy.")]
    public string EnemyName = "";
    [Tooltip("The max life the enemy can have.")]
    public int MaxLife = 100;

    public const float DistanceToFollow = 7f;

    [Tooltip("The type of the enemy.")]
    public EnemyType Type = EnemyType.None;

    //We use this to control the life.
    protected float currentLife;

    //We use this for reference.
    protected GameObject _PlayerGameObject;
    protected PlayerLife _playerLife;

    protected BaseEnemyMovement _EnemyMovement;

    //We use this to control the state of our enemy.
    public EnemyStates _EnemyState = EnemyStates.Idle;

    //We use this to store what we want to run on the update.
    protected delegate void BaseEnemyUpdate();
    protected BaseEnemyUpdate _BaseUpdate;

    //We use this to store waht we want to run on the FixedUpdate.
    protected delegate void BaseEnemyFixedUpdate();
    protected BaseEnemyFixedUpdate _BaseFixedUpdate;

    #region Protected Methods

    #region Mono
    
    protected virtual void Awake()
    {
        _EnemyMovement = GetComponent<BaseEnemyMovement>();
        _EnemyMovement.EnemyMovementAwake();

        //We use this to cache the basic stuff for every enemy.
        _PlayerGameObject = GameObject.FindGameObjectWithTag("Player");
        _playerLife = _PlayerGameObject.GetComponent<PlayerLife>();

    }

    protected virtual void Start() { }

    protected virtual void Update()
    {
        //We use this to control the FSM.
        ControlStates();

        if (_BaseUpdate != null)
        {
            _BaseUpdate();
        }
    }

    protected virtual void FixedUpdate()
    {
        if (_BaseFixedUpdate != null)
        {
            _BaseFixedUpdate();        
        }
    }

    #endregion

    //We control life only when we get hit, not every frame.
    protected virtual void ControlLife()
    {
        if (currentLife <= 0)
        {
            Death();    
        }
    }

    protected virtual void Death()
    {
        //We shoul do pooling stuff here.
        Destroy(this.gameObject);
    }

    //This is a test, we are overriding this in the specifi logic for each type of enemy.
    protected virtual void ControlStates() { }
    
    //When we need to change the state we need to call this so we deal with the delegate stuff only here.
    protected virtual void ChangeEnemyState(EnemyStates stateToChange) { }

    //We dont need to override this one.
    protected void ClearBaseDelegates()
    {
        _BaseFixedUpdate = null;
        _BaseUpdate = null;
    }

    //We use this to get the distance from the player.
    protected float CheckPlayerDistance()
    {
        return Vector3.Distance(this.gameObject.transform.position, _PlayerGameObject.transform.position);
    }

    //We use this only for debug, we actually should put this in another class so we don't build with the final game.
    protected void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.gameObject.transform.position, DistanceToFollow);
    }

    #endregion

    #region Public Methods

    public void ApplyDamage(float damage)
    {
        Debug.Log("We are applying damage to the enemy!", this.gameObject);
        currentLife += damage;

        //We call this when we apply damage to evaluate the enemy life.
        ControlLife();
    }

    #endregion
}