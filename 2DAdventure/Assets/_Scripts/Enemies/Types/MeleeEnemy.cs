﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeleeAttack), typeof(BaseEnemyMovement))]
public class MeleeEnemy : BaseEnemy
{   
    [Header("MeleeEnemy variables.")]
    [Tooltip("The distance to the player to initialize the attack.")]
    public float DistanceToAttack = 3f;

    [Tooltip("The enemy will patrol or not.")]
    public bool Patrol = true;

    protected MeleeAttack _EnemyMeleeAttack;

    protected override void Awake()
    {
        base.Awake();

        EnemyName = "MeleeEnemy";
        Type = EnemyType.Melee;

        _EnemyMeleeAttack = GetComponent<MeleeAttack>();
    }

    //ACtually we dont need to override this two methods because we change just the delegate.
    protected override void Update()
    {
        base.Update();
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    //We need to override this entirely.
    protected override void ChangeEnemyState(EnemyStates stateToChange)
    {
        Debug.Log("Changing enemy movement state, FROM:" + _EnemyState + " / TO: " + stateToChange);

        //We clear the delegates methods.
        ClearBaseDelegates();

        //We add new methods to the delegate based in what State we are going.
        switch (stateToChange)
        {
            case EnemyStates.Idle:
                ChangeEnemyState(EnemyStates.Movement);
                break;

            case EnemyStates.Movement:
                _BaseUpdate += _EnemyMovement.EnemyMovementUpdate;
                _BaseFixedUpdate += _EnemyMovement.EnemyMovementFixedUpdate;
                break;

            case EnemyStates.MeleeAttack:
                _BaseUpdate += _EnemyMeleeAttack.MeleeAttackUpdate;
                break;

            case EnemyStates.None:
                ChangeEnemyState(EnemyStates.Idle);
                break;
        }

        //We actualize the state.
        _EnemyState = stateToChange;
    }

    //We need to override this entirely.
    protected override void ControlStates()
    {
        //TODO: Take this off.
        if (_EnemyState != EnemyStates.Movement && _EnemyState != EnemyStates.MeleeAttack)
        {
            //Debug.Log("Setup the initial state. Why do we have this?");
            //This is the initial setup.
            if (_EnemyState == EnemyStates.Idle)
            {
                ChangeEnemyState(EnemyStates.Movement);
                
                //If we patrol, we go to patrol otherwise we just go to follow player.
                //FollowPlayer probly will never be the default one, so we can kind of delete this part of code.
                if (Patrol)
                {
                    if (_EnemyMovement._EnemyMovementState != EnemyMovement.Patrol)
                    {
                        _EnemyMovement.ChangeMovementStatus(EnemyMovement.Patrol);
                    }
                }
                else if (!Patrol && _EnemyMovement._EnemyMovementState != EnemyMovement.FollowTarget)
                {
                    _EnemyMovement.ChangeMovementStatus(EnemyMovement.FollowTarget);
                }
            }   
        }

        //If the distance to the player is higher than the distance to attack we decide what type of movement we are doing.
        if (CheckPlayerDistance() > DistanceToAttack && _EnemyState != EnemyStates.Movement)
        {
            //Debug.Log("The distance to the player is higher than the distance to attack.");
            
            //TODO: I think the problem is something with the Movement State and when we change to Follow and Patrol.
            if (CheckPlayerDistance() < DistanceToFollow && _EnemyMovement._EnemyMovementState != EnemyMovement.FollowTarget)
            {
                //Debug.Log("We are going to FollowTarget movement.");
                ChangeEnemyState(EnemyStates.Movement);

                _EnemyMovement.ChangeMovementStatus(EnemyMovement.FollowTarget);
            }
            else if (CheckPlayerDistance() >= DistanceToFollow && _EnemyMovement._EnemyMovementState != EnemyMovement.Patrol)
            {
                //Debug.Log("We are going to Patrol movement.");
                ChangeEnemyState(EnemyStates.Movement);

                _EnemyMovement.ChangeMovementStatus(EnemyMovement.Patrol);
            }
        }
        else if (CheckPlayerDistance() < DistanceToAttack && _EnemyState != EnemyStates.MeleeAttack)
        {
            //Debug.Log("The distance to the player is lower than the distance to attack.");
            //Debug.Log("We are going to the Attack state.");

            ChangeEnemyState(EnemyStates.MeleeAttack);
        }
    }
}