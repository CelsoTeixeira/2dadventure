﻿using UnityEngine;
using System.Collections;

//This enemy will be stationary because I'm really lazy rigth now.

[RequireComponent(typeof(RangedAttack), typeof(BaseEnemyMovement))]
public class RangedEnemy : BaseEnemy
{
    public float DistanceToAttack = 6f;

    public bool Patrol = false;

    protected RangedAttack _EnemyRangedAttack;

    protected override void Awake()
    {
        base.Awake();

        EnemyName = "RangedEnemy";
        Type = EnemyType.Ranged;

        _EnemyRangedAttack = GetComponent<RangedAttack>();
        
        ChangeEnemyState(EnemyStates.RangedAttack);
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    protected override void ChangeEnemyState(EnemyStates stateToChange)
    {
        ClearBaseDelegates();

        switch (stateToChange)
        {
            case EnemyStates.RangedAttack:
                _BaseUpdate += _EnemyRangedAttack.RangedAttackUpdate;
                break;
        }
    }

    protected override void ControlStates()
    {

    }
}
