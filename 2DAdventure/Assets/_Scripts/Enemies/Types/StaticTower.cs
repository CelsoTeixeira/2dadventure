﻿using UnityEngine;
using System.Collections;

public enum TowerState
{
    None,
    Cooldown,
    Attacking
};

public class StaticTower : MonoBehaviour
{
    public float Range = 10f;
    public float TimeToWait = 2f;

    public float Damage;

    public TowerState State = TowerState.None;

    private bool _canAttack = true;
    private float _internalTimer = 0f;

    private bool _internalAttackCondition = false;

    private GameObject _playerGameObject;
    private PlayerLife _playerLife;

    [Header("Gizmos")]
    public Color GizmosColor = Color.red;

    protected virtual void Awake()
    {
        if (_playerGameObject == null)
            _playerGameObject = GameObject.FindGameObjectWithTag("Player");

        _playerLife = _playerGameObject.GetComponent<PlayerLife>();
    
        
    }

    protected virtual void Update()
    {
        //Lets do a delegate to train.

    }

    #region Internal Switcher

    protected virtual void None() { }

    protected virtual void Cooldown()
    {
        _internalTimer += Time.deltaTime;

        if (_internalTimer >= TimeToWait)
        {
            _canAttack = true;
        }
    }
    
    protected virtual void Attacking() { }

    protected virtual void TowerBehaviour() { }

    #endregion

    //TODO: We can move this to a helper class.
    protected virtual bool CheckForPlayerInRange()
    {
        if (Vector3.Distance(this.gameObject.transform.position, _playerGameObject.transform.position) < Range)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = GizmosColor;
        Gizmos.DrawWireSphere(this.gameObject.transform.position, Range);
    }
}
