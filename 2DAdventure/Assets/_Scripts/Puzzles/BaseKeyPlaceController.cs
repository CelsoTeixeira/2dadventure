﻿using UnityEngine;
using System.Collections;

//TODO: We need to implement virtual stuff for this class if we need.

public class BaseKeyPlaceController : MonoBehaviour
{
    public bool HaveKey { get { return haveKey; } private set { } }

    [Tooltip("The reference to the right door we are going to open.")]
    public GameObject DoorToOpenGO;

    [Tooltip("The gameObject we are going to use to hold the particle.")]
    public GameObject ParticleToStart;

    //We are making this equal to the global key.
    private KeyCode ActionKeyCode;   

    private bool haveKey = false;
    private int KeyNumber;  //This should be the same number from the key.
                            //This is refreshed in the GotKey method.

    protected bool Used = false;        //Internal control to see if we already played a animation or not.
    protected BaseDoor DoorController;
    protected Animator animator;

    #region Mono

    void Awake()
    {
        animator = GetComponent<Animator>();

        if (DoorToOpenGO != null)
        {
            DoorController = DoorToOpenGO.GetComponent<BaseDoor>();
        }

    }

    void Start()
    {
        //We have a default KeyCode for Action Stuff.
        //We need to access the Instance in Start because we are seting it on Awake().
        ActionKeyCode = KeyBindsSetup.Instance.ActionKeyCode;    
    }

    #endregion

    #region Public access

    //This can be private maybe.
    public void TriggerAnimation()
    {
        animator.SetTrigger("PutKey");
    }

    //When we collide with a key, we call this from the TriggerEnter in the KeyGameObject.
    public void GotKey(int keyReference)
    {
        KeyNumber = keyReference;
        haveKey = true;
    }
    
    #endregion

    #region Collision / Trigger

    protected virtual void OnTriggerEnter2D(Collider2D col) { }
    protected virtual void OnTriggerExit2D(Collider2D col) { }

    protected virtual void OnTriggerStay2D(Collider2D col)
    {
        if (haveKey)
        {
            if (!Used)
            {
                if (Input.GetKeyDown(ActionKeyCode))
                {
                    //Debug.Log("We are going to trigger the animation and change var in the right door.");
                    //We are triggering the animation and in the animation we are calling the OpenDoorByAnimation() to open our door.
                    TriggerAnimation();

                    //DoorController.ChangeCondition(true, KeyNumber);
                    //We change our internal so we don't play the animation after we have already opened the door.
                    Used = true;
                }
            }
            else if (Used)
            {
                //We are proble doing nothing here.
                //We could do some effect, like a dissolve or something after the animation is done.
                //Debug.Log("We have already opened the door.");
            }
        }
    }

    //We are going to call this from the AnimationEvent, it's a initialy test.
    public void OpenDoorByAnimation()
    {
        //If we have a GameObject to play particle we go inside, otherwise we are just opening the door.
        if (ParticleToStart != null)
        {
            ParticleSystem system = ParticleToStart.GetComponent<ParticleSystem>();

            //If there's an ParticleSystem in the reference provided we play the particle.
            if (system != null)
            {
                system.Play();
            }
        }

        //Opening the door from the reference.
        DoorController.ChangeCondition(true, KeyNumber);
    }

    #endregion
}