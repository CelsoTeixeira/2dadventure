﻿using UnityEngine;
using System.Collections;

//TODO:Clean Debug.Log().

public class BaseKey : MonoBehaviour
{
    [Header("Base Key components")]
    [Tooltip("What door we are going to unlock with this key.")]
    public GameObject DoorGO;
    [Tooltip("If we just get the key and it open the door or we need to place the key in the keyhole before.")]
    public bool OpenDoorDirect = true;
    [Tooltip("If OpenDoorDirect = false, we need the keyPlace gameObject we are going to use to open the door.")]
    public GameObject KeyPlaceGO;
    [Tooltip("The tag we are comparing OnTriggerEnter.")]
    public string PlayerTag = "Player";
    [Tooltip("The number of this key in the sequence to unlock the door.")]
    public int KeyNumber = 0;   //We need to begin in 0 here because arrays start on 0!

    protected BaseDoor DoorController;
    protected BaseKeyPlaceController KeyPlaceController;

    #region Mono

    protected virtual void Awake()
    {
        //We are going to get references here.
        //We don't need to get both references!
        if (OpenDoorDirect) //If we open the door we get the reference for the door. If not we don't need the reference.
        {
            DoorController = DoorGO.GetComponent<BaseDoor>();
        }
        else if (!OpenDoorDirect)    //If we doesn't open the door directly we need to get the right reference.
        {
            KeyPlaceController = KeyPlaceGO.GetComponent<BaseKeyPlaceController>();
        }
    }

    #endregion

    #region Collision / Trigger

    //We are going to let this virtual to override if we need in the future class.
    protected virtual void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag(PlayerTag))
        {

            //First we check if we are going to open the door direct or not.
            if (OpenDoorDirect) //If we are just opening the door.
            {
                //We need a reference to the player to tell we got a key, or we can just open the right door, I think this can be better.
                if (DoorController != null)
                {
                    Debug.Log("We are trying to change some bool condition in the BaseDoor.");
                    DoorController.ChangeCondition(true, KeyNumber);

                    //We are going to destroy the object to start, but we could do some pooling here.
                    Destroy(this.gameObject);
                }                
            }
            else if (!OpenDoorDirect)
            {
                //We need to change some value in the BaseKeyPlaceController to inform we have the right key.
                if (KeyPlaceController != null)
                {
                    Debug.Log("We are going to say we have the right key to the KeyPlaceController.", KeyPlaceGO);
                    KeyPlaceController.GotKey(KeyNumber);

                    Destroy(this.gameObject);
                }
            }
        }
    }

    protected virtual void OnTriggerExit2D(Collider2D col) { }
    protected virtual void OnTriggerStay2D(Collider2D col) { }

    #endregion
}
