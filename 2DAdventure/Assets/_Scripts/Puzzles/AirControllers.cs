﻿using UnityEngine;
using System.Collections;

public class AirControllers : MonoBehaviour
{
    [Tooltip("The holding gameObject for WorkParticle.")]
    public GameObject WorkParticlesGameObject;
    [Tooltip("The holding gameObject for StopParticle.")]
    public GameObject StopParticleGameObject;
    
    //We use this to internal track the state of our variable.
    private bool Active = false; //The default will be False.

    private Effector2D effector2D;
    private ParticleSystem _particleSystemWork;
    private ParticleSystem _particleSystemStop;

    void Awake()
    {
        effector2D = GetComponent<AreaEffector2D>();
        effector2D.enabled = false;

        _particleSystemWork = WorkParticlesGameObject.GetComponent<ParticleSystem>();
        _particleSystemWork.Stop();

        _particleSystemStop = StopParticleGameObject.GetComponent<ParticleSystem>();
        _particleSystemStop.Play();
    }

    #region Public methods

    public void AirControllerHandler()
    {
        if (Active)
        {
            DeActiveAirTunnel();
        }
        else if (!Active)
        {
            ActiveAirTunnel();
        }
    }

    #endregion

    #region Private methods

    private void ActiveAirTunnel()
    {
        Active = true;

        _particleSystemStop.Stop();
        _particleSystemWork.Play();
        
        effector2D.enabled = true;
    }

    private void DeActiveAirTunnel()
    {
        Active = false;

        _particleSystemWork.Stop();
        _particleSystemStop.Play();

        effector2D.enabled = false;
    }

    #endregion
}
