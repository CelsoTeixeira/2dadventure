﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BaseDoor : MonoBehaviour
{
    public int LevelToLoad = 0;

    [Header("Base Door components")]
    [Header("Sprite properties")]
    [Tooltip("The sprite to give visual feedback when the door is open.")]
    public Sprite openDoorSprite;
    [Tooltip("The sprite to give visual feedback when the door is closed.")]
    public Sprite closeDoorSprite;
    
    [Header("Key components")]
    [Tooltip("The door uses key to unlock?")]
    public bool UseKey = true;      //We are going to be setting the Key to be the default way to open doors.
    [Tooltip("The amount of keys we need to unlock the door.")]
    public int AmountOfKeys = 1;
    
    //We are going to change this, so the player will need to go to the KeyPlace and put the key there.
    public bool[] HaveKeys;     //We should make this private or something, we really don't need this to bo public.
    
    [Header("Lever components")]
    [Tooltip("The door uses lever to unlock?")]
    public bool UseLever = false;
    [Tooltip("The amount of levers we need to unlock the door.")]
    public int AmountOfLevers = 1;
    
    public bool[] LeversActivates;      //This will be private when we are done testing!

    protected bool HaveAllKeys = false;
    protected bool AllLeversActivates = false;

    protected SpriteRenderer DoorSpriteRenderer;

    #region Protected Methods

    protected virtual void Awake()
    {
        DoorSpriteRenderer = GetComponentInChildren<SpriteRenderer>();
    }

    protected virtual void OpenDoor()
    {
        if (UseKey)
        {
            KeysLoop();
        }
        else if (UseLever)
        {
            LeversLoop();
        }
    }

    protected virtual void LeversLoop()
    {
        for (var x = 0; x <= LeversActivates.Length; x++)
        {
            if (!LeversActivates[x])
            {
                AllLeversActivates = false;
                break;
            }
            else if (LeversActivates[x])
            {
                AllLeversActivates = true;
            }
        }
    }

    protected virtual void KeysLoop()
    {
        for (var x = 0; x <= HaveKeys.Length - 1; x++)
        {
            //If we don't have a key, we just set AllKeys to false we break from the function.
            if (HaveKeys[x] == false)
            {
                HaveAllKeys = false;
                break;
            }
            else if (HaveKeys[x] == true)
            {
                HaveAllKeys = true;
            }
        }
    }

    #endregion

    #region Public Methods

    //We call this on the Key Trigger stuff, need to get the right reference.
    public void ChangeCondition(bool boolToChange, int arrayNumber, bool useKey = true)
    {
        //Debug.Log("We are changing the bool condition for the key/lever using the parameters passed!", this.gameObject);

        if (useKey)
        {
            //Check the lenght so we don't get bad limits. TODO: We need better check here.
            if (arrayNumber < AmountOfKeys)
            {
                HaveKeys[arrayNumber] = boolToChange;
            }
        }
        else    //If we don't use a key we enter here.
        {
            if (arrayNumber < AmountOfLevers)
            {
                LeversActivates[arrayNumber] = boolToChange;
            }
        }

        //We only need to check this when we change some bool inside our array.
        OpenDoor();

        if (HaveAllKeys)
        {
            ChangeSprite(true);
        }
    }

    #endregion

    //doorCondition = false, we display the close door sprite.
    void ChangeSprite(bool doorCondition)
    {
        if (DoorSpriteRenderer == null)
            return;

        if (doorCondition)
        {
            DoorSpriteRenderer.sprite = openDoorSprite;
        }
        else if (!doorCondition)
        {
            DoorSpriteRenderer.sprite = closeDoorSprite;
        }
    }


    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            if (HaveAllKeys)
            {
                Debug.Log("We have all the keys, we are going to load the next level or change the position.");
                Application.LoadLevel(LevelToLoad);

            }
            else
            {
                Debug.Log("We don't have the right keys.");
            }
        }
        
    }
}
