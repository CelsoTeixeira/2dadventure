﻿using UnityEngine;
using System.Collections;

public class LeverAirController : BaseLever
{
    private AirControllers controller;

    protected override void Awake()
    {
        controller = ObjectToUnlock.GetComponent<AirControllers>();
    }

    protected override void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.CompareTag(PlayerTag))
        {
            if (Input.GetKeyDown(ActionKeyCode))
            {
                //TODO: Need to test.
                controller.AirControllerHandler();
            }
        }
    }
}