﻿using UnityEngine;
using System.Collections;

//We are going to use this to activate platforms.

public class BaseLever : MonoBehaviour
{
    [Header("Base Lever components")]
    [Tooltip("What door we are going to unlock with this lever.")]
    public GameObject ObjectToUnlock;
    [Tooltip("The tag we are comparing OnTriggerEnter")]
    public string PlayerTag = "Player";

    protected KeyCode ActionKeyCode;

    #region Mono

    protected virtual void Awake() { }

    protected virtual void Start()
    {
        //Getting the default KeyCode here.
        ActionKeyCode = KeyBindsSetup.Instance.ActionKeyCode;
    }

    #endregion

    #region Collision / Trigger

    protected virtual void OnTriggerEnter2D(Collider2D col) { }
    protected virtual void OnTriggerExit2D(Collider2D col) { }
    protected virtual void OnTriggerStay2D(Collider2D col) { }

    #endregion
}
