﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

using Pathfinding;

//This is only working with horizontal movement yet.


public class WaypointMovement : MonoBehaviour
{
    public float MovementSpeed = 10f;
    public float MaxHorizontalSpeed = 5f;

    public float DistanceToNextPoint = 1f;

    public List<Transform> WaypointsHolder;
    private Vector3 _currentWaypoint;

    private int _currentIndex;

    private Seeker _seeker;
    private Path _path;

    private Transform _transform;
    private Rigidbody2D _rigidbody2D;

    private Vector3 _dir;

    void Awake()
    {
        _transform = GetComponent<Transform>();
        _rigidbody2D = GetComponentInParent<Rigidbody2D>();
        _seeker = GetComponent<Seeker>();

        //We registre the callback method here.
        _seeker.pathCallback += OnPathComplete;

        _currentWaypoint = WaypointsHolder[0].position; //We set the first waypoint
    }

    public void WaypointMovementFixedUpdate()
    {
        if (_path == null)
            return;

        //We are dealing with the movement now.
        _dir = (_path.vectorPath[_currentIndex] - _transform.position).normalized;
        _dir *= MovementSpeed * Time.fixedDeltaTime;

        _rigidbody2D.MovePosition(_transform.position + _dir);

        //_rigidbody2D.AddForce(_dir, ForceMode2D.Impulse);
         
        //if (_rigidbody2D.velocity.x > MaxHorizontalSpeed)
        //    _rigidbody2D.velocity = new Vector2(MaxHorizontalSpeed, _rigidbody2D.velocity.y);

        //Cheking for distance.
        if (Vector3.Distance(_transform.position, _path.vectorPath[_currentIndex]) < DistanceToNextPoint)
        {
            //Checking for index and size of vectorPath.
            if (_currentIndex < _path.vectorPath.Count - 1)
            {
                _currentIndex ++;
            }
            else
            {        
                //We could do a CoRoutine here so before we start a new Path we can wait for a few seconds.
                WaypointController();
            }
        }
    }

    private void WaypointController()
    {
        _path = null;

        _rigidbody2D.velocity = Vector2.zero;

        //We change the _currentWaypoint.
        _currentWaypoint = (_currentWaypoint == WaypointsHolder[0].position)
            ? WaypointsHolder[1].position
            : WaypointsHolder[0].position;

        //We reset our index.
        _currentIndex = 0;

        //We ask for a new path.
        _seeker.StartPath(_transform.position, _currentWaypoint);
    }

    public void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            _path = p;  //Assign the path.

            _currentIndex = 0; //Reset the index.
        }
    }


    //TODO: We are using this just to start the movement.
    public void TriggerMovement()
    {
        _seeker.StartPath(_transform.position, _currentWaypoint);
    }

    public void OnDisable()
    {
        _seeker.pathCallback -= OnPathComplete;
    }
}