﻿using UnityEngine;
using System.Collections;

using Pathfinding;


public class FollowTargetMovement : MonoBehaviour
{
    public float MovementSpeed = 10f;
    public float MaxHorizontalSpeed = 5f;

    public float DistanceToNextPoint = 1f;

    public GameObject TargetToFollow;

    //This is the rate we are recalling the path.
    public float RepathRate = 0.5f;

    private int _currentIndex;  //We use this to control where we're going.

    private Seeker _seeker;
    private Path _path;

    private Transform _transform;
    private Rigidbody2D _rigidbody2D;

    private Vector3 _dir;

    //If we can ask for a Path again or not.
    public bool _canSearchAgain = true;

    public float _canSearchTimer;

    #region Mono

    private void Awake()
    {
        if (TargetToFollow == null)
        {
            TargetToFollow = GameObject.FindGameObjectWithTag("Player");
        }

        _transform = GetComponent<Transform>();
        _rigidbody2D = GetComponentInParent<Rigidbody2D>();
        _seeker = GetComponent<Seeker>();

        _seeker.pathCallback += OnPathComplete;
    }

    //Easy access to the methods we need to call for the movement stuff.
    public void FollowMovementUpdate()
    {
        //This is used to refresh the current path for a new one.
        //Need to be called in the Update method from MonoBehaviour.
        HandleSearchTimer();
    }

    public void FollowMovementFixedUpdate()
    {
        //This is used to update the current position based in the NodeGraph form the A* project.
        //We are dealing with rigidbody, so this should be called in the FixedUpdate from MonoBehaviour.
        HandleMovement();
    }

    #endregion

    private void HandleMovement()
    {
        if (_path == null || TargetToFollow == null)
            return;

        //Debug.Log("Index: " + _currentIndex);

        //TODO: We need to do this correct.
        //This handles movement.
        _dir = (_path.vectorPath[_currentIndex] - _transform.position).normalized;
        _dir *= MovementSpeed * Time.fixedDeltaTime;

        _rigidbody2D.MovePosition(_transform.position + _dir);

        //_rigidbody2D.AddForce(_dir, ForceMode2D.Impulse);

        //if (Mathf.Abs(_rigidbody2D.velocity.x) > MaxHorizontalSpeed)
        //{
        //    _rigidbody2D.velocity = new Vector2(MaxHorizontalSpeed, _rigidbody2D.velocity.y);
        //}

        if (Vector3.Distance(_transform.position, _path.vectorPath[_currentIndex]) < DistanceToNextPoint)
        {
            if (_currentIndex < _path.vectorPath.Count - 1)
            {
                _currentIndex++;
            }
            else
            {
                //Debug.Log("We have reached the final step in the vectorPath.");
            }
        }
    }

    private void HandleSearchTimer()
    {
        //If we can search we dont need to do anything here.
        if (_canSearchAgain)
            return;

        //Debug.Log("We are using the timer.");

        _canSearchTimer += Time.deltaTime;  //Increase the timer.

        if (_canSearchTimer > RepathRate)   //If the timer is higher than the repathRate.
        {
            //Debug.Log("We can search again. We are going to call for a new path.");
            _canSearchAgain = true; //We can search again.

            GetNewPath();   //We ask for a new path.
        }

    }

    private void GetNewPath()
    {
        if (!_canSearchAgain)
        {
            Debug.Log("We cant ask for a new path because we canSearch again.");
            return;
        }

        //Debug.Log("We are getting a new path!");
        //We can't search again if we just asked for a new path.
        _canSearchAgain = false;
        _canSearchTimer = 0f;

        //We get the new path.
        _seeker.StartPath(_transform.position, TargetToFollow.transform.position);
    }

    public void OnPathComplete(Path p)
    {
        //Why do we need to release the path?

        if (!p.error)
        {
            _path = p;  //Assign the path.

            //_currentIndex = 0;  //Reset the index.
        }
    }

    public void OnDisable()
    {
        //If we're inactive we dont need to be callback anymore.
        _seeker.pathCallback -= OnPathComplete; 
    }

    public void OnEnable()
    {
        _seeker.pathCallback += OnPathComplete;
    }

    public void TriggerMovement()
    {
        GetNewPath();
    }
}