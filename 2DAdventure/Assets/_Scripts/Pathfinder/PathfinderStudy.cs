﻿using Pathfinding;
using UnityEngine;
using System.Collections;

public class PathfinderStudy : MonoBehaviour
{
    public Vector3 TargetPosition;      //This is just to initialize movement.
    public float MovementSpeed;
    public float NextWaypointDistance = 1f; //The minimun distance we need to be before we move to next index.

    private int _currentWaypointIndex;  //Our index to control where we are in the array.

    private Seeker _seeker;  
    private Path _path;

    private Transform _transform;
    private Rigidbody2D _rigidbody2D;

    void Awake()
    {
        _transform = GetComponent<Transform>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _seeker = GetComponent<Seeker>();

        _seeker.pathCallback += OnPathComplete; //We define what method will be called when we receive a new path.
    }

    void Start()
    {
        _seeker.StartPath(_transform.position, TargetPosition);
    }

    void FixedUpdate()
    {
        if (_path == null)
        {
            return;
        }


        Vector3 dir = (_path.vectorPath[_currentWaypointIndex] - _transform.position).normalized;
        dir *= MovementSpeed * Time.fixedDeltaTime;

        _rigidbody2D.AddForce(dir, ForceMode2D.Impulse);

        
        if (Vector3.Distance(_transform.position, _path.vectorPath[_currentWaypointIndex]) < NextWaypointDistance)
        {
            if (_currentWaypointIndex < _path.vectorPath.Count - 1)
            {
                _currentWaypointIndex ++;
                Debug.Log("We are increasing the index to keep looping.");
            }
            else
            {
                Debug.Log("We reached the final point, we are not going to increase, we could reset variables here.");

                //We are reseting here.
                _path = null;
                _currentWaypointIndex = 0;
            }
        }
    }

    //Path Callback, this is called when we ask for a new path from seeker.StartPath
    //and the seeker sends the path with this method.
    //So we check for errors, if there's none we just assign the path received to be 
    //our path and we reset the index we use to loop through the path.vectorPath.
    public void OnPathComplete(Path p)
    {
        //Debug.Log("We just receive our path!");
        //Debug.Log("Path Details: " + p.vectorPath);
        //Debug.Log("Path Size: " + p.vectorPath.Count);

        //foreach (var vp in p.vectorPath)
        //{
        //    Debug.Log("Path Positions: " + vp);
        //}

        if (!p.error)
        {
            _path = p;

            _currentWaypointIndex = 0;
        }
    }

    void OnDisable()
    {
        _seeker.pathCallback -= OnPathComplete;
    }
}
