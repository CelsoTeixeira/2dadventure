﻿using UnityEngine;
using System.Collections;

public class GenerateLocation : MonoBehaviour
{
    //Do I really need this one?
    public Location LocationToGenerate { get; private set; }

    private Location _locationToGenerate;

    #region Limits
    //  On the X limits we want to do some access to the location, like a bridge or something just to add visuals.
    private int _minimunXLimit; //The minimun X we can get.
    private int _maximunXLimit; //The maximun X we can get.

    private int _maximunYLimit; //This is our maximun Y.
    #endregion

    public void StartGeneration(Location location)
    {
        //We also need to get max and minimun location on the X.

        //We want to start with the initial tiles here, based in the location.Size;
    
        //If we do a 1x1 tile we can do a simply for for loop for x and y. 
        //The first x will be our first limit and the last one will be our second limit.
        //On Y we need a maximun int .
        //The tiles can be transparent because we are going to add the background on the fly and the terrain too.
    }
}