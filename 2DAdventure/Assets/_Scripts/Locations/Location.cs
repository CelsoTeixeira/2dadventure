﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Location : ScriptableObject
{
    public string LocationName;     //The name of the location, we will use to create a entry in the dictionary with this.
    public string FactionName;  //Do we want diferents Factions in the same world?

    public int LocationSize;            //We are going to randon generate the location based in this size.
    public Sprite[] LocationBuildings;  //What sprites we are going to use in the location.

    //We use this to control specific buildings that will appear in the city.
    public bool HasStore;
    public bool HasArmory;
    public bool HasCityHall;

    public int NumberOfPeople;      //How many people we have in the location.
    public string[] NameOfPeople;   //We proble want some random generate name here to add weird names on the people.
}