﻿using UnityEngine;
using System.Collections;

public static class GetClosestEnemy 
{
    //This should get some GameObject.
    public static GameObject ClosestEnemy(Transform center, float radius, string TagToCheck, int layerToBitShift)
    {
        LayerMask layerToCheck = 1 << layerToBitShift;  //We need to bit shifting the layer to work.

        //TODO: Change this to a OverlapNonAlloc to save some memory.
        Collider2D[] overlapCollider2D = Physics2D.OverlapCircleAll(center.position, radius, layerToCheck);

        if (overlapCollider2D.Length != 0)
        {
            //We need to change this to a forloop.
            for (var x = 0; x < overlapCollider2D.Length; x++)
            {
                if (overlapCollider2D[x].gameObject.CompareTag(TagToCheck))
                {
                    //TODO: If we have the right check we put this in a new list to check for distance.
                    //How can we sort this overlap cirlce to get the enemy with the closest distance from the player.
                    Debug.Log("This is the first enemy we got in the OverlapCircle, we are returning it but would be nice if we can sort it someway.", overlapCollider2D[x].gameObject);
                    
                    return overlapCollider2D[x].gameObject;
                }
            }
        }

        //If overlapCollider is null, we return null, this means we doesn't have any enemy in the radius.
        return null;
    }
}