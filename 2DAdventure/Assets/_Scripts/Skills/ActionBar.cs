﻿using UnityEngine;
using System.Collections;

/*  We need to get control of the images and UI stuff, then we just refresh it to update icons.
 */

public class ActionBar : MonoBehaviour
{
    #region Singleton 
    
    public static ActionBar Instance { get; private set; }

    private void SingletonSetup()
    {
        if (Instance != this && Instance != null)
            Destroy(gameObject);

        Instance = this;
    }

    #endregion

    public BaseSkill[] SkillsOnActionBar = new BaseSkill[4];

    private ProjectileSkill projectileSkillReference;
    private BasicAttack basicAttack;

    #region Mono

    void Awake()
    {
        SingletonSetup();

        //WE NEED TO GET FURTHER REFERENCE FOR THE SKILLS BASED IN IT'S TYPE.

        ////TODO: This is kind working now.
        //foreach (var baseSkill in SkillsOnActionBar)
        //{
        //    Debug.Log("Base Skill name: " + baseSkill.SkillName);
        //    Debug.Log("Base Skill type: " + baseSkill.SkillType);

        //    if (baseSkill.SkillType == SkillType.Projectile)
        //    {
        //        Debug.Log("Our base type is a projectile, we need to get this reference now.");
        //        projectileSkillReference = baseSkill.GetComponent<ProjectileSkill>();
        //        projectileSkillReference.TestDebug();
        //    }
        //    else if (baseSkill.SkillType == SkillType.Test2)
        //    {
        //        Debug.Log("Our base type is a Test2, we need to get this reference now.");
        //    }
        //}
    }

    #endregion

    #region Public Acess

    public void PressActionBar(int actionBarSlot)
    {
        Debug.Log("We are pressing the action bar n°: " + actionBarSlot);

        if (SkillsOnActionBar[actionBarSlot] == null)
        {
            Debug.Log("We don't have any Skill in the slot: " + actionBarSlot);
        }
        else
        {
            Debug.Log("We are going the get the right references here to launch the skill.");
        }
    }

    //TODO: We need to test this stuff...
    //We get a int for reference where in the action bar we are going to add the skill and the base skill to add.
    //0, 1, 2, 3 - 0 we are making it be the basic attack, maybe we can allow the player to don't use the basic attack in the future.
    public void AddSkillOnActionBar(BaseSkill skillToAdd, int actionBarSlot)
    {
        Debug.Log("We are adding a new skill in the actionBar, with the name: " + skillToAdd.SkillName);
        SkillsOnActionBar[actionBarSlot] = skillToAdd;
        
        //We need to actualize the reference here too.
        //Every time we change any skill in the actionBar we are going to refresh the script associate to the skill.
    }

    #endregion
}