﻿using UnityEngine;
using UnityEngine.UI;

//This is how we take track for the ActionBar.
public enum ActionSkill
{
    None,
    Fireball,
    Iceball,
    WindBlast
};

public enum SkillElement
{
    None,
    Fire,
    Ice,
    Wind
};

//We need to use this to sort the type of the skill.
public enum SkillType
{
    None,
    Projectile,
    Test2
};

//This will be the base for every skill in the game.
public abstract class BaseSkill : MonoBehaviour
{
    #region Public Fields
    [Header("Basic skill status")]
    [Tooltip("The name of the skill.")]
    public string SkillName = "";
    [Tooltip("What we're going to instantiate.")]
    public GameObject SkillPrefab;
    [Tooltip("How much time we wait until the skill can be used again.")]
    public int SkillCooldown = 2;
    [Tooltip("The damage the skill will apply.")]
    public float SkillDamage = 10f;
    [Tooltip("The name of the skill that we will use in the ActionBar. To change/add need to go to the enum.")]
    public SkillType SkillType = SkillType.None;
    [Tooltip("What element the skill is associated?")]
    public SkillElement SkillElement = SkillElement.None;
    
    [Header("Basic skill UI information")]
    [Tooltip("The icon we will show in the UI")]
    public Image SkillIcon;

    //We are using this to control if we the skill is ready to be used or not.
    protected bool SkillReady = true;
    protected float CooldownTimer;

    #endregion

    #region Mono Behaviours

    protected virtual void Awake() { }

    //We should do some cooldown calculation here.
    protected virtual void Update() { } 

    #endregion

    #region Protected Methods

    //We do calculations stuff here.
    protected virtual void SkillDamageCalculations() { }

    //We control the cooldown timer here.
    protected virtual void SkillCooldownHandler()
    {
        //If we can use the skill, we dont need to run this function.
        if (SkillReady)
        {
            return;
        }

        CooldownTimer += Time.deltaTime;    //Increase over time.

        if (CooldownTimer >= SkillCooldown)     //If our timer is higher than the Cooldown, we are done here.
        {
            SkillReady = true;  //We set the bool to true.

            CooldownTimer = 0;  //We reset the timer to 0, when we cast the skill and we set the bool to false the cooldown handler will just run.
        }
    }

    #endregion

    #region Public methods

    //This don't need to be public, we should call this only in the damage, inside the attack.
    //Maybe I need to pass this damage to the skill gameObject?
    public float SkillDamageAmount()
    {
        //We first check what will be our damage.
        SkillDamageCalculations();

        //Then we return it.
        return SkillDamage;
    }

    #endregion
}