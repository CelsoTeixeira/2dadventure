﻿/*  This will handle initialization of skills.
 *  The movement of prjectile skills will be handlded by the physics system.
 *  The collsion checking is in another script that only handles Trigger/Collision and take care of particles.
 */

using UnityEngine;
using System.Collections;

public class ProjectileSkill : BaseSkill
{
    #region Public Fields

    [Header("Internal control")]
    [Tooltip("The amount of how much of the same skill we have in the scene.")]
    public int MaximunAmountOfProjectiles = 3;

    [Header("Force modifiers / Rigidbody modifiers")] 
    [Tooltip("What type of force we should apply?")]
    public bool IsImpulse = true;
    [Tooltip("What type of force we should apply?")]
    public bool IsForce = false;
    [Tooltip("The amount of force we will apply")]
    public Vector2 ImpulseVector2;

    [Header("Movement modifiers")]
    //This should affect the movement speed.
    [Tooltip("Should we have air resistance?")]
    public bool AirResistence = false;
    [Tooltip("The amount of air resistene we have.")]
    public float AirResistenceAmount = 0f;
    [Tooltip("We are considering Unity's Gravity ?")]
    public bool UseGravity = true;
    
    private Rigidbody2D _rigidbody2D;
    private PlayerLife _playerLife;     //We should transform this in some base entity, so we can use the same code for player and enemies.

    #endregion

    #region Mono

    protected override void Awake()
    {
        base.Awake();

        _rigidbody2D = GetComponent<Rigidbody2D>();
        _playerLife = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerLife>();
    
        ForceCheck();
    }



    #region Damage Calculation
    //TODO: Think what we want to do here, what we will evaluate.
    //Each type of skill will have a different calculation.
    protected override void SkillDamageCalculations()
    {
        float newDamage = 0;

        newDamage += SkillDamage;

        //Now we do all the calculations we need.

        SkillDamage = newDamage;
    }

    #endregion

    #endregion

    #region Private Methods

    //We can't have IsImpulse and IsForce true, so we need to check this to prevent error.
    private void ForceCheck()
    {
        //We will always chose IsImpulse if we need to choise between both
        if (IsImpulse && IsForce)
        {
            IsImpulse = true;
        }
    }

    #endregion

    #region Public Methods
    //Need to pay atention to set the vector with the right direction! We need to get the player direction for this.

    //We call this after we instantiated the projectile to launch it.
    public void LaunchProjectile()
    {
        _rigidbody2D.AddForce(ImpulseVector2, ForceMode2D.Impulse);
    }
    
    //We can try to do some charge launch, like, the amount of time pressed will affect
    //the velocity. 
    public void LaunchProhectile(Vector2 forceVector2)
    {
        _rigidbody2D.AddForce(forceVector2, ForceMode2D.Impulse);
    }

    //TODO: Debug
    public void TestDebug()
    {
        Debug.Log("Testing reference!");
    }

    #endregion
}