﻿using System.Reflection;
using UnityEngine;
using System.Collections;

public enum BasicAttackStatus
{
    None,
    Cooldown,       //We can do some effect with this state to give some visual feedback.
    GoingToPosition, //We go to the right position here.
    ReadyForAttack, //We stay at the right place and we give more visual feedback.
    Attack          //We go to the target position and perform the attack.
};

public class BasicAttack : BaseSkill
{
    [Header("Basic Attack properties")] [Tooltip("The range of the basic attack.")] 
    public float BasicAttackRange = 5f;

    [Tooltip("The cooldown between each basic attack we can do.")] 
    public float BasicAttackCooldown = 1f;
        //Maybe we can handle this like a global cooldown.

    [Tooltip("The speed we are going to use to go to positions.")] 
    public float BasicAttackMovementToPositonSpeed = 3f;

    [Tooltip("The speed we are goind to use to go to the target position.")] 
    public float BasicAttackMovementToTargetSpeed = 6f;

    [Header("Basic Attack GameObject and specifics")] [Tooltip("The game object we have for the basic attack.")] 
    public GameObject BasicAttackGameObject;

    [Tooltip("This should be a child in the mainChar, we use this to be the position when we are ready to attack.")] 
    public GameObject BasicAttackReadyToAttackPosition;

    //We probly don't need this here.
    [Tooltip("What particle we will use for the basic attack.")] 
    public GameObject BasicAttackParticle;

    [Tooltip("We use this to control the status of the BasicAttack. This probly should be private!")]
    public BasicAttackStatus AttackStatus = BasicAttackStatus.None;

    private bool CanAttack = true;

    //We are using this to get a reference for the transform of the basic attack gameObject.
    private Transform basicAttackTransform;

    private GameObject attackTargetGameObject;
    private Transform attackTarget;

    //We are going to use a delegate to control the states...
    private delegate void BasicStateControl();

    private BasicStateControl myStateControl;

    //We use this to track if we are inside the coroutine or not. 
    private bool insideCooldown = false;
    private bool insideGoToPosition = false;
    private bool insideAttack = false;

    #region Mono

    protected override void Awake()
    {
        base.Awake();

        basicAttackTransform = BasicAttackGameObject.GetComponent<Transform>();
    }

    protected override void Update()
    {        
        //Debug.Log("Basic Attack Status: " + AttackStatus);


        StatusControl();
    }

    #endregion

    #region Public Methods

    public void AssignTarget(GameObject target)
    {
        Debug.Log("We are assign the target.", target.gameObject);

        //We just assign the attackTarget here.
        attackTargetGameObject = target;
        attackTarget = attackTargetGameObject.transform;

        if (attackTargetGameObject == null || target == null)
            Debug.Log("We dont have a proper target, its all null.");

        //Try to attack?
        //If we can attack we start the final check.
        if (AttackStatus == BasicAttackStatus.ReadyForAttack)
        {
            //If we are not inside the coroutine we start it.
            if (!insideAttack)
            {
                StartCoroutine(HandleAttack());
            }
        }
        else
        {
            //Debug.Log("We are not ready to attack.");
        }
    }

    #endregion

    #region Private Methods

    //We control the internal status here.
    internal void StatusControl()
    {
        switch (AttackStatus)
        {
            case BasicAttackStatus.Cooldown:

                StayOnReadyToAttackPosition();

                if (!insideCooldown)
                {
                    StartCoroutine(HandleCooldown());
                }

                break;

            case BasicAttackStatus.GoingToPosition:

                if (!insideGoToPosition)
                {
                    StartCoroutine(HandleGoingToPosition());
                }

                break;

            case BasicAttackStatus.ReadyForAttack:
                HandleReadyToAttack();

                break;

            case BasicAttackStatus.Attack:
                HandleAttack();

                break;
        }
    }

    private IEnumerator HandleCooldown()
    {
        //Debug.Log("We are starting the Cooldown Coroutine.");
        insideCooldown = true; //We change the internal control here.

        //We don't need CanAttack anymore probly.
        if (CanAttack)
        {
            AttackStatus = BasicAttackStatus.ReadyForAttack;
            yield break;
        }

        AttackStatus = BasicAttackStatus.Cooldown; //We change the State to cooldown.

        yield return new WaitForSeconds(BasicAttackCooldown); //We wait for the cooldown timer.

        AttackStatus = BasicAttackStatus.GoingToPosition; //We change the State to GoingToPosition.

        CanAttack = true;

        insideCooldown = false; //We are leaving this coroutine.
        yield return null;
        //Debug.Log("We are leaving the Cooldown coroutine.");
    }

    private IEnumerator HandleGoingToPosition()
    {
        //Debug.Log("We are starting the GoingToPosition coroutine.");

        insideGoToPosition = true; //We are inside the coroutine.

        //While the basicAttack position is not equal the target position, we keep moving.
        do
        {
            float step = BasicAttackMovementToPositonSpeed*Time.deltaTime;
            basicAttackTransform.position = Vector3.MoveTowards(basicAttackTransform.position,
                BasicAttackReadyToAttackPosition.transform.position, step);

            yield return new WaitForEndOfFrame();
        } while (basicAttackTransform.position != BasicAttackReadyToAttackPosition.transform.position);

        if (CanAttack)
        {
            AttackStatus = BasicAttackStatus.ReadyForAttack;
        }
        else if (!CanAttack)
        {
            AttackStatus = BasicAttackStatus.Cooldown;
        }

        insideGoToPosition = false;
        yield return null;
        //Debug.Log("We are leaving the GoingToPosition coroutine.");

    }

    private IEnumerator HandleAttack()
    {
        //Debug.Log("We are entering the HandleAttack coroutine.");
        insideAttack = true; //We are changing the internal control here.
        AttackStatus = BasicAttackStatus.Attack;

        //If we dont have an target, we leave this coroutine.
        if (attackTarget == null)
        {
            AttackStatus = BasicAttackStatus.ReadyForAttack;
            insideAttack = false;
            //Debug.Log("We are leaving the HandleAttack because we dont have a proper target.");
            yield break;
        }

        do
        {
            float step = BasicAttackMovementToTargetSpeed*Time.deltaTime;
            basicAttackTransform.position = Vector3.MoveTowards(basicAttackTransform.position, attackTarget.position,
                step);
            yield return new WaitForEndOfFrame();
        } while (basicAttackTransform.position != attackTarget.position);

        PerformAttack();

        AttackStatus = BasicAttackStatus.GoingToPosition;
        insideAttack = false;
        yield return null;
        //Debug.Log("We are leaving the handle attack.");
    }

    private void HandleReadyToAttack()
    {
        StayOnReadyToAttackPosition();

        //Debug.Log("We are waiting for Input to start the attack.");
    }

    private void PerformAttack()
    {
        CanAttack = false;

        Debug.Log("We are doing damage here.");

        //We check to see if the targetAttackGameObject is null.
        //We already checked for this shit.

        //We get the reference for the enemy life.
        //We need a global reference for this.

        //We apply the damage.

    }

    private void StayOnReadyToAttackPosition()
    {
        //We just set the basic attack transform to be the same of the readytoattack position.
        BasicAttackGameObject.transform.position = BasicAttackReadyToAttackPosition.transform.position;

    }

#endregion

    #region Internal

    internal void ChangeStatus(BasicAttackStatus status)
    {
        Debug.Log("We are changing the BasicAttackStatus: " + status, this.gameObject);
        AttackStatus = status;
    }

    #endregion


    #region Utility / Gizmos

    public void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, BasicAttackRange);
    }

    #endregion
}