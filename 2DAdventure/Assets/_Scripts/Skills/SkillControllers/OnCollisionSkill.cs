﻿/*  We are just using this to check for collisions and spawn particles.
 */ 

using UnityEngine;
using System.Collections;

public class OnCollisionSkill : MonoBehaviour
{
    [Header("GameObject specifics")]
    [Tooltip("The particle we will spawn when we have Player impact.")]
    public GameObject ParticleOnPlayerImpact;
    [Tooltip("The particle we will spawn when we have Enviroment impact.")]
    public GameObject ParticleOnEnviromentImpact;
    [Tooltip("The gameObject that contains the sprite renderer component.")]
    public GameObject SpriteGameObject;

    [Header("Tag Check")]
    public string PlayerTag = "Player";
    public string EnviromentTag = "Enviroment";
    public string EnemyTag = "Enemy";

    [Header("Collider specific")]
    [Tooltip("The collider needs to be trigger?")]
    public bool IsTrigger = true;       //We are going to set the default to be true for trigger.

    //References
    private Collider2D collider2D;
    
    //We are going to need to know the skill type and get the right reference to decrease projectile
    //number when we are about to destroy this gameObject.
    //How do we do this?


    #region Mono

    void Awake()
    {
        //Just a remenber here!
        if (PlayerTag == null || EnviromentTag == null || EnemyTag == null)
        {
            Debug.Log("Set the tag's correct!", this.gameObject);
        }

        //We just check if the isTrigger bool is true/false and set the trigger component in the 
        //collider to be equal it.
        TriggerCheck();
    }

    #endregion 

    #region Collision / Trigger

    //We are going to set the basic to be OnTrigger, if we need to be OnCollision we can refactor this stuff.

    public void OnTriggerEnter2D(Collider2D other)
    {
        //We are checking for the tag's here.
        if (other.gameObject.CompareTag(PlayerTag))
        {
            //We need to call our main skill component do to some calcs before we apply damage on the player.
        }

        if (other.gameObject.CompareTag(EnviromentTag))
        {
            //We start the impact collision.
        }

        if (other.gameObject.CompareTag(EnemyTag))
        {
            //We do some blood splash?        
            Destroy(other.gameObject, 0.2f);
        }
    }

    public void OnTriggerExit2D(Collider2D other) { }
    public void OnTriggerStay2D(Collider2D other) { }

    #endregion

    #region Trigger Control

    private void TriggerCheck()
    {
        //We can check here if the collider is Trigger or not, and set it.
        collider2D = GetComponent<Collider2D>();
        collider2D.isTrigger = IsTrigger;
    }

    #endregion
}