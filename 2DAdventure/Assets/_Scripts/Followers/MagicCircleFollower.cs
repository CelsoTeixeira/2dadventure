﻿using UnityEngine;

/*  This will handle some kind of circle that will follows the 
 *  player around and attack when some input is pressed.
 *  The attack will be:
 *      Leave position near player.
 *      Go to target position.
 *      Spawn some particle on target hit.
 *      Come back to the player.
 *      
 *  The Idle State we just keep circling the player position.
 *  We probly need a pivot point for this, inside the player maybe...
 */

public class MagicCircleFollower : BasicFollower 
{
   
}
