﻿using UnityEngine;
using System.Collections;

public enum FollowerStatus
{
    None,
    FollowingPlayer,
    Attacking,
    CatchigPlayer,
    Disabled
};

public abstract class BasicFollower : MonoBehaviour 
{
    [Header("Follower basic properties")]
    [Tooltip("The name of the follower.")]
    public string FollowerName = "";

    //Maybe we do a delegate to control?

    protected virtual void ControlFollower() { }


}