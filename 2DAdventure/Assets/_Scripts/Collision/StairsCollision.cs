﻿using UnityEngine;

public class StairsCollision : MonoBehaviour
{
    private PlayerMovement _playerMovement; //This is for reference.

    //TODO: We should make a global stuff to handle this initialization for us, to load everythign at once.
    
    void Awake()
    {
        _playerMovement = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            _playerMovement.OnStair(true);
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            _playerMovement.OnStair(false);
        }
    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            //Maybe we should do a if check here to not call this every interation OnStay.
            _playerMovement.OnStair(true);
        }
    }
}