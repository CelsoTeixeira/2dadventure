﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ConversationManager : MonoBehaviour 
{
    #region Singleton SetUp

    public static ConversationManager Instance { get; private set; }

    private void SingletonSetUp()
    {
        if (Instance != this && Instance != null)
            Destroy(gameObject);

        Instance = this;
    }

    #endregion

    #region UI Public Fields

    public Text UICharacterNameText;
    public Text UIConversationLines;
    public Image UIDisplayPic;

    public GameObject UIConversationPanel;

    public Font FontStyle;

    #endregion

    #region Private Fields

    //Is there any conversation going on?
    private bool talking = false;

    //The current line of text beign displayed
    private ConversationEntry currentConversationLine;

    private GameObject NPC;

    #endregion

    #region Mono

    void Awake()
    {
        SingletonSetUp();
        FontSetUp();   
    }

    void Update()
    {
        UIControl();
    }

    #endregion

    #region Public Fields

    public void StartConversation(Conversation conversation, GameObject NPC_Holder)
    {
        NPC = NPC_Holder;   //We cache this for reference later.

        //Start displaying the supplied conversation
        if (!talking)
        {
            StartCoroutine(DisplayConversation(conversation));
        }
    }

    #endregion

    #region Private Methods

    private IEnumerator DisplayConversation(Conversation conversation)
    {
        talking = true;

        foreach (var conversationLine in conversation.ConversationLines)
        {
            currentConversationLine = conversationLine;

            //conversationTextWidth = currentConversationLine.ConversationText.Length*fontSpacing;
            yield return new WaitForSeconds(3);
        }

        talking = false;
        
        if (NPC != null)
        {
            //We just refresh our internal control in the NPC here.
            NPC.GetComponent<NPCTalk>().ActualizeInternalControl(talking);
        }
    }

    private void UIControl()
    {
        if (!talking)
        {
            //If the conversationPanel is active, we deactive it.
            if (UIConversationPanel.activeInHierarchy != false)
                UIConversationPanel.SetActive(false);

            return;
        }

        //If the conversationPanel is deactive, we active it.
        if (UIConversationPanel.activeInHierarchy != true)
            UIConversationPanel.SetActive(true);

        //We change the UI name to the speaking character here.
        UICharacterNameText.text = currentConversationLine.SpeakingCharacterName;

        //We change the UI conversation line here.
        UIConversationLines.text = currentConversationLine.ConversationText;

        //If we have a display pic to show in the conversation, we actualize it.
        if (currentConversationLine.DisplayPic != null)
            UIDisplayPic.overrideSprite = currentConversationLine.DisplayPic;
    }

    //We set up the font we are going to use here!
    private void FontSetUp()
    {
        if (FontStyle == null)
            return;

        UIConversationLines.font = FontStyle;
        UICharacterNameText.font = FontStyle;
    }

    #endregion
}