﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public class ConversationEntry
{
    public string SpeakingCharacterName;
    public string ConversationText;
    public Sprite DisplayPic;
}