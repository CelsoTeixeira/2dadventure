﻿using UnityEngine;
using System.Collections;

//Any better way to do this?

public class CanvasStayOnPlayer : MonoBehaviour
{
    public GameObject PlayerPosition;

    private Transform transform;

    void Awake()
    {
        transform = GetComponent<Transform>();
    }

    void LateUpdate()
    {
        if (PlayerPosition != null)
        {
            transform.position = PlayerPosition.transform.position;        
        }
    }
}
