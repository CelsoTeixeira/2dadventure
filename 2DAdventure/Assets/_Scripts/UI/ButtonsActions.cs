﻿using UnityEngine;
using System.Collections;

public class ButtonsActions : MonoBehaviour 
{
    public void LoadLevel(int levelToLoad)
    {
        Application.LoadLevel(levelToLoad);
    }

    public void TogglePanel(GameObject panelToToggle)
    {
        panelToToggle.SetActive(!panelToToggle.activeInHierarchy);
    }

    public void Exit()
    {
        Application.Quit();
    }


}