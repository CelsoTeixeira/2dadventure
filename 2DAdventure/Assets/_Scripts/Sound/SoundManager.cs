﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{
    public AudioSource BackgroundAudioSource;

    public List<AudioClip> BackgroundAudioClips;

    //We track the index to use on the List to make sure we know what clip we are playing.
    private int currentIndex = 0;

    void Start()
    {
        //We assign the audio clip.
        BackgroundAudioSource.clip = BackgroundAudioClips[0];
        //We play the audio.
        BackgroundAudioSource.Play();
    }

    void Update()
    {
        //This will make kind of a loop to keep the sound playing.
        if (!BackgroundAudioSource.isPlaying)
        {
            //Refresh the clip from the source.
            BackgroundAudioSource.clip = BackgroundAudioClips[currentIndex];
            //Play the audio source.
            BackgroundAudioSource.Play();
        }
    }

    //We use this to change the background sound if we need.
    public void ChangeBackgroundSound(int numberInTheArray)
    {
        //We stop the sound.
        BackgroundAudioSource.Stop();

        //Make sure we are inside the List limits.
        if (numberInTheArray > BackgroundAudioClips.Count || numberInTheArray < 0)
        {
            numberInTheArray = BackgroundAudioClips.Count - 1;
        }

        //We refresh the index.
        currentIndex = numberInTheArray;

        //We refresh the clip.
        BackgroundAudioSource.clip = BackgroundAudioClips[currentIndex];
        //We play the clip.
        BackgroundAudioSource.Play();
    }
}