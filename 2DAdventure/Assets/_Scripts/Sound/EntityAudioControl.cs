﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class EntityAudioControl : MonoBehaviour
{
    public List<AudioClip> ClipsToPlay;

    private AudioSource _audioSource;

    void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    //We can ask for a AudioClip or for a int to get the AudioClip from the List.
    public void TriggerSound(AudioClip clipToPlay)
    {
        if (_audioSource != null)
        {
            Debug.Log("Trying to play a one shot on the audio source.");
            _audioSource.PlayOneShot(clipToPlay);        
        }
    }
}