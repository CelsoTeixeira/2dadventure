﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum ItenType
{
    None,
    Potion,
    Weapon,
    Armour
};

public class BaseIten : MonoBehaviour
{
    [Header("Iten Specifics")]
    [Tooltip("The name of the iten.")]
    public string Name = "";
    [Tooltip("What type is the iten.")]
    public ItenType Type = ItenType.None;

    [Header("GUI Specifics")]
    [Tooltip("The icon we will display with the icon.")]
    public Image ItenIcon;
}
