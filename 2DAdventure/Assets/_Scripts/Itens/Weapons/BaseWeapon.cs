﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

/*  Enemies should drop shit, including weapons, and armours? Should Weapon and Armor devire from the same class?
 *  We could have consumables, like potions to restore health/mana and give buffs.
 */

public enum WeaponType
{
    None,
    Sword,
    Staff
};

public enum WeaponElement
{
    None,
    Fire,
    Ice
};

public class BaseWeapon : BaseIten
{
    [Header("Weapon specifics")] 
    [Tooltip("The base damage we're going to use to do calculations.")]
    public float WeaponBaseDamage = 10f;
    [Tooltip("What type is the weapon")]
    public WeaponType WeaponType = WeaponType.None;
    [Tooltip("What element the weapon have.")]
    public WeaponElement WeaponElement = WeaponElement.None;   //We could use some multipliers here if the enemy has weakness.
    [Tooltip("The 3D model we are going to be using with the weapon.")]
    public GameObject WeaponModel;
    
    #region Mono

    protected virtual void Awake()
    {
        Type = ItenType.Weapon;     //Just make sure the type is Weapon.
    }

    #endregion



}
