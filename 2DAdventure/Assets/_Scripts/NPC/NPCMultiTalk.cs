﻿using UnityEngine;
using System.Collections;

//TODO: Develop this better.
public class NPCMultiTalk : NPCTalk
{
    private int CurrentIndex = 0;

    protected override void StartDialog()
    {
        if (Talking)
            return;

        Debug.Log("We are starting the dialog.", this.gameObject);
        ConversationManager.Instance.StartConversation(ConversationsHolder[CurrentIndex], this.gameObject);
    }

    public void ChangeDialog(int index)
    {
        Debug.Log("We are changing the Index for the conversation.");
        CurrentIndex = index;
    }
}
