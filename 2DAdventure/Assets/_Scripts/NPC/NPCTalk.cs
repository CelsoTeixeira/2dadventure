﻿using UnityEngine;
using System.Collections;

public class NPCTalk : MonoBehaviour
{
    public Conversation[] ConversationsHolder;

    public bool Talking = false;

    protected int _currentConversationIndex = 0;

    private KeyCode actionKeyCode;

    protected virtual void Start()
    {
        actionKeyCode = KeyBindsSetup.Instance.ActionKeyCode;
    }

    protected virtual void StartDialog()
    {
        //We need a internal control here to not start a new conversation everytime we call this.

        if (Talking)
            return;

        Debug.Log("We are starting the dialog.", this.gameObject);
        ConversationManager.Instance.StartConversation(ConversationsHolder[0], this.gameObject);
    }

    public void ActualizeInternalControl(bool toChange)
    {
        Talking = toChange;
    }

    //Trigger
    protected virtual void OnTriggerEnter2D(Collider2D col)
    {
        //We show something here.
    }

    protected virtual void OnTriggerExit2D(Collider2D col)
    {
        //We hide the GUI display here.
    }

    protected virtual void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            if (!Talking)
            {
                Debug.Log("We can start the dialog here.");

                if (Input.GetKeyDown(actionKeyCode))
                {
                    StartDialog();
                }
            }
        }
    }
}
