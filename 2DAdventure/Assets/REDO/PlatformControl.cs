﻿using UnityEngine;
using System.Collections;

public class PlatformControl : MonoBehaviour
{
    public bool PlatformActive = false;

    private FollowPath followPath;

    void Awake()
    {
        followPath = GetComponent<FollowPath>();
        
        followPath.PlatStart();
    }

    void Update()
    {
        if (PlatformActive)
        {
            followPath.PlatUpdate();
        }
    }
}
