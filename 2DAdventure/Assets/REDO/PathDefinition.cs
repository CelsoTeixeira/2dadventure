﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class PathDefinition : MonoBehaviour {

	public Transform[] points;


	public IEnumerator<Transform> GetPathEnumarator() {
		if (points == null || points.Length < 1) yield break;

		int direction = 1;
		int index     = 0;
		while (true) {
			yield return points[index];

			if (points.Length == 1) continue;

			if (index <= 0)
				direction = 1;
			else if (index >= points.Length - 1)
				direction = -1;

			index += direction;
		}
	}


	public void OnDrawGizmos() {
		if (points == null || points.Length < 2) return;

		Gizmos.color = Color.green;
		for (int i = 1; i < points.Length; i++) {
			Gizmos.DrawLine(points[i - 1].position, points[i].position);
		}
	}
}
