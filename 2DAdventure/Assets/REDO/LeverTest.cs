﻿using UnityEngine;
using System.Collections;

public class LeverTest : MonoBehaviour
{
    public GameObject PlatformToActive;

    private PlatformControl platformControl;

    void Awake()
    {
        platformControl = PlatformToActive.GetComponent<PlatformControl>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            Debug.Log("We are activating the platform, should ask for input.");
            platformControl.PlatformActive = true;
        }
    }
}
