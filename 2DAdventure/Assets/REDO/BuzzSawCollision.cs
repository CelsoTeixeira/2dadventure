﻿using UnityEngine;
using System.Collections;


public class BuzzSawCollision : MonoBehaviour
{

    public int Damage = 10;

    public float RotationSpeed = 2f;

    //Store this is a static class.
    public string TagToCheck = TagsInfo.PlayerTag;
    
    private PlayerLife _playerLife;
    private Transform _transform;
    
    void Awake()
    {
        _playerLife = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerLife>();
        _transform = GetComponent<Transform>();
    }

    void Update()
    {        
        _transform.Rotate(new Vector3(_transform.localRotation.x, _transform.localRotation.y,
            RotationSpeed));
    }

    public void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag(TagToCheck))
        {
            _playerLife.ApplyDamage(-Damage);
            
        }
    }

    public void OnCollisionEnter2D(Collision2D col)
    {
        
    }
}