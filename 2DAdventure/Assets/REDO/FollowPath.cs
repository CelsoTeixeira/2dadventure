﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FollowPath : MonoBehaviour {

	public enum FollowType { MoveTowards, Lerp }
	public FollowType type = FollowType.MoveTowards;

	public PathDefinition path;
	public float          speed                = 1;
	public float          MAX_DISTANCE_TO_GOAL = 0.1F;

	private float SQUARED_MAX_DISTANCE_TO_GOAL;
	private IEnumerator<Transform> currentPoint;

	public void PlatStart()
    {
		if (path == null) 
        {
			Debug.LogError("Path cannot be null ", gameObject);
			return;
		}

		currentPoint = path.GetPathEnumarator();
		currentPoint.MoveNext();
		if (currentPoint.Current == null) return;

		transform.position = currentPoint.Current.position;

		SQUARED_MAX_DISTANCE_TO_GOAL = MAX_DISTANCE_TO_GOAL * MAX_DISTANCE_TO_GOAL;
	}

	public void PlatUpdate() {
		if (currentPoint == null || currentPoint.Current == null) return;

		if (type == FollowType.MoveTowards)
			transform.position = Vector3.MoveTowards(transform.position, currentPoint.Current.position, Time.deltaTime * speed);
		else
			transform.position = Vector3.Lerp(transform.position, currentPoint.Current.position, Time.deltaTime * speed);

		float distance = (transform.position - currentPoint.Current.position).sqrMagnitude;

		if (distance <= SQUARED_MAX_DISTANCE_TO_GOAL) currentPoint.MoveNext();
	}
}
