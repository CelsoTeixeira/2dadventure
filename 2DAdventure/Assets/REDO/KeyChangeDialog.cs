﻿using UnityEngine;
using System.Collections;

//TODO: Maybe a better way to do this.

public class KeyChangeDialog : BaseKey
{
    public GameObject NPC;
    public int ConversarionIndexToGo = 0;

    private NPCMultiTalk _multiTalk;

    protected override void Awake()
    {
        base.Awake();

        _multiTalk = NPC.GetComponent<NPCMultiTalk>();

    }

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag(PlayerTag))
        {

            //First we check if we are going to open the door direct or not.
            if (OpenDoorDirect) //If we are just opening the door.
            {
                //We need a reference to the player to tell we got a key, or we can just open the right door, I think this can be better.
                if (DoorController != null)
                {
                    Debug.Log("We are trying to change some bool condition in the BaseDoor.");
                    DoorController.ChangeCondition(true, KeyNumber);

                    _multiTalk.ChangeDialog(ConversarionIndexToGo);

                    //We are going to destroy the object to start, but we could do some pooling here.
                    Destroy(this.gameObject);
                }
            }
            else if (!OpenDoorDirect)
            {
                //We need to change some value in the BaseKeyPlaceController to inform we have the right key.
                if (KeyPlaceController != null)
                {
                    Debug.Log("We are going to say we have the right key to the KeyPlaceController.", KeyPlaceGO);
                    KeyPlaceController.GotKey(KeyNumber);

                    _multiTalk.ChangeDialog(ConversarionIndexToGo);

                    Destroy(this.gameObject);
                }
            }
        }
    }
}
