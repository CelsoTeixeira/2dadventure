﻿using UnityEngine;
using System.Collections;

public class WinCondition : MonoBehaviour
{
    public GameObject WinText;
    public PauseControl _pauseControl;

    void Awake()
    {
        _pauseControl = GameObject.FindGameObjectWithTag("Manager").GetComponent<PauseControl>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            Debug.Log("Congratulations, you won!");
            
            WinText.SetActive(true);
            _pauseControl.Pause();
        }
    }
}
