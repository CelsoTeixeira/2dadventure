﻿using UnityEngine;
using System.Collections;

public class PauseControl : MonoBehaviour
{
    public GameObject PausePanel;

    private bool _pause = false;

    void Awake()
    {
    
        PausePanel.SetActive(false);

        Time.timeScale = 1;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {    
            Pause();
        }
    }

    public void Pause()
    {
        _pause = !_pause;
        
        if (_pause)
        {
            Time.timeScale = 0;
            PausePanel.SetActive(true);
        }
        else if (!_pause)
        {
            Time.timeScale = 1;
            PausePanel.SetActive(false);
        }
    }
}