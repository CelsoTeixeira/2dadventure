﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {
	public  Transform player;
	public  float     speed;
	public  int       numberOfBackgrounds;
	public  Transform background;
	private float     minX;
	private float     maxX;


	public void Start() {
		minX = GameObject.FindWithTag("MainCamera").GetComponent<Camera>().orthographicSize * 1.9F;
		maxX = 1000;
	}


	public void LateUpdate() {
		Vector3 newPos     = new Vector3(player.position.x, player.position.y, transform.position.z);
		transform.position = Vector3.Lerp(transform.position, newPos, speed * Time.deltaTime);
		//float x            = transform.position.x;
		//x                  = Mathf.Clamp(x, minX, maxX);
		//transform.position = new Vector3(x, transform.position.y, transform.position.z);
	}
}
