﻿using UnityEngine;
using System.Collections;

public class RetardSplashScreen : MonoBehaviour
{
    public GameObject MainMenuPanel;
    public GameObject SplasScreenPanel;

    public float TimeToWait = 3f;

    void Start()
    {
        MainMenuPanel.SetActive(false);
        SplasScreenPanel.SetActive(true);

        StartCoroutine(SplashScreenTimer());        
    }

    private IEnumerator SplashScreenTimer()
    {
        MainMenuPanel.SetActive(false);
        SplasScreenPanel.SetActive(true);

        yield return new WaitForSeconds(TimeToWait);

        MainMenuPanel.SetActive(true);
        SplasScreenPanel.SetActive(false);
    }
}