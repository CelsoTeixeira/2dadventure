﻿using UnityEditor;
using UnityEngine;
using System.Collections;

public class ConversationAssetCreator : MonoBehaviour 
{
    [MenuItem("Assets/Create/CustomAssets/Conversation")]
    public static void CreateAsset()
    {
        CustomAssetUtility.CreateAsset<Conversation>();
    }
}
