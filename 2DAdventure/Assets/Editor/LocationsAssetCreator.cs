﻿using UnityEditor;
using UnityEngine;
using System.Collections;

public class LocationsAssetCreator : MonoBehaviour 
{
    [MenuItem("Assets/Create/CustomAssets/Location")]
    public static void CreateAsset()
    {
        CustomAssetUtility.CreateAsset<Location>();
    }
}
